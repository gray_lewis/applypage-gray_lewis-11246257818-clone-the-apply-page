import React, { Component } from 'react'
import { ServerStyleSheet } from 'styled-components'

export default {
  siteRoot: 'https://www.codersclan.com',
  devServer: {
    port: 8080,
  },
  getSiteData: () => ({
    title: 'CodersClan - The worldwide coder community to kickoff your freelance career',
    description: 'Join a global community of coders and work as much as you want from the comfort of your home',
    links: {
      applyFrontend: '/frontend',
      applyWordpress: '/wordpress',
      applyAnimator: '/animator',
      terms: '/terms',
      privacy: '/privacy-policy',
      facebook: 'https://www.facebook.com/CodersClan',
      twitter: 'https://twitter.com/CodersClan',
      airfleet: 'https://www.airfleet.co',
      agreementSuccess: '/agreement-success',
      wordpressTypeForm: 'https://codersclan.typeform.com/to/yiyAyg',
      frontendTypeForm: 'https://codersclan.typeform.com/to/xIjAlg',
      animatorTypeForm: 'https://codersclan.typeform.com/to/NGzyS7',
      agreementWebhook: 'https://hooks.zapier.com/hooks/catch/394547/04eh6h/'
    },
  }),
  getRoutes: async () => {
    return [
      {
        path: '/',
        component: 'src/containers/Home',
        getData: () => ({
          navbar: 'white',
        }),
      },
      {
        path: '/terms',
        component: 'src/containers/Terms',
      },
      {
        path: '/privacy-policy',
        component: 'src/containers/Privacy',
      },
      {
        path: '/wordpress',
        component: 'src/containers/Apply',
        getData: async () => ({
          variation: await "wordpressTypeForm"
        }),
      },
      {
        path: '/frontend',
        component: 'src/containers/Apply',
        getData: async () => ({
          variation: await "frontendTypeForm"
        }),
      },
      {
        path: '/animator',
        component: 'src/containers/Apply',
        getData: async () => ({
          variation: await "animatorTypeForm"
        }),
      },
      {
        path: '/agreement/wordpress',
        component: 'src/containers/Agreement',
        noindex: true,
        getData: async () => ({
          variation: await "wordpress"
        }),
      },
      {
        path: '/agreement/frontend',
        component: 'src/containers/Agreement',
        noindex: true,
        getData: async () => ({
          variation: await "frontend"
        }),
      },
      {
        path: '/agreement-success',
        component: 'src/containers/AgreementSuccess',
        noindex: true,
      },
      {
        is404: true,
        component: 'src/containers/404',
      },
    ]
  },
  renderToHtml: (render, Comp, meta) => {
    const sheet = new ServerStyleSheet();
    const html = render(sheet.collectStyles(<Comp />));
    meta.styleTags = sheet.getStyleElement();
    return html
  },
  Document: class CustomHtml extends Component {
    render () {
      const
        {
          Html, Head, Body, children, siteData, renderMeta,
        }
          = this.props;

      return (
        <Html>
          <Head>
            <meta charSet="UTF-8" />
            <title>{siteData.title}</title>
            <meta name="description" content={siteData.description} />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <link rel="icon" type="image/ico" href="https://www.codersclan.com/favicon.ico" />
            <meta property="og:title" content={siteData.title}/>
            <meta property="og:description" content={siteData.description} />
            <meta property="og:image" content="https://www.codersclan.com/codersclan_og_new.jpg" />
            <meta property="og:url" content="https://www.codersclan.com/" />
            <meta property="og:type" content="website" />
            <meta name="twitter:card" content="summary_large_image" />
            { renderMeta.styleTags }
          </Head>
          <Body>{ children }</Body>
        </Html>
      )
    }
  },
}
