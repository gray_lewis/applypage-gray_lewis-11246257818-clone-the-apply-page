export const Colors = {
  BLUE: "#1C71B8",
  GREEN: "#1CB78D",
  ORANGE: "#FF911E",
  LIGHT_GRAY: "#F5F5F5",
  LIGHT_BLACK: "#4A4A4A",
  WHITE: "#FFFFFF"
};