import React from 'react'
import { Router } from 'react-static'
import { injectGlobal } from 'styled-components'
import { hot } from 'react-hot-loader'
import reset from 'styled-reset';
//
import Routes from 'react-static-routes'
import AppStyles from 'containers/AppStyles'
import Navbar from 'components/Navbar'
import Footer from 'components/Footer'

// Needed for loading the our WebFonts form TypeKit
if (typeof window !== 'undefined' || typeof document !== 'undefined') {
  const WebFont = require('webfontloader')
  WebFont.load({
    google: {
      families: ['Roboto:300,400,500,700'],
    },
  })
  // Insert query string params into the typeform
  const setIframe = setInterval(() => {
    if (document.getElementById('typeform-full') != null && window.location.search.length > 0) {
      const checkVar = new RegExp(/\?/)
      const hasQuestionMark = checkVar.test(document.getElementById('typeform-full').src)
      const queryVars = hasQuestionMark ? '&'+window.location.search.split('?')[1] : window.location.search
      document.getElementById('typeform-full').src = document.getElementById('typeform-full').src + queryVars
      clearInterval(setIframe)
    }
  }, 100)
}

injectGlobal`
  ${ reset }
  body {
    font-family: 'Roboto', sans-serif;
    font-weight: 400;
    background-color: #fff;
  }
`;

const App = () => (
  <Router>
    <AppStyles>
      <Navbar />
      <Routes />
      <Footer />
    </AppStyles>
  </Router>
);

export default hot(module)(App)
