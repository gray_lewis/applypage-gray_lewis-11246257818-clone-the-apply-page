import React from 'react'
import styled from 'styled-components';
import { Link, withSiteData } from 'react-static';
import { Colors } from "../consts/styles";
//
import fbIcon from '../svg/icon_sn_facebook.svg';
import twitterIcon from '../svg/icon_sn_twitter.svg';

const Footer = styled.footer`
  display: flex;
  justify-content: center;
  padding: 2rem 0;

  .social-icons{
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    
    a+ a {
      margin-left: 1rem;
    }
    
    img{
      width: 1.25rem;
      height: 1.25rem;
    }
  }
  
  .link-list{
    display: flex;
    align-items: center;
    justify-content: center;
    margin-top: 1.5rem;
    
    @media (max-width: 415px){
      flex-direction: column;
    }
    
    li{
      color: ${ Colors.LIGHT_BLACK };
       
      a{
        text-decoration: none;
        color: ${ Colors.ORANGE };
      }
      & + li{
        margin-left: 1rem;
        
        @media (max-width: 415px){
          margin: 1rem 0 0 0;
        }
      } 
    }
  }
`;

export default withSiteData(({links}) => (
  <Footer>
    <div className="container">
      <div className="social-icons">
        <Link to={links.twitter} target="_blank">
          <img src={twitterIcon} alt=""/>
        </Link>
        <Link to={links.facebook} target="_blank">
          <img src={fbIcon} alt=""/>
        </Link>
      </div>
      <ul className="link-list">
        <li>
          © {(new Date).getFullYear()} CodersClan
        </li>
        <li>
          <Link to={links.terms}>Terms</Link>
        </li>
        <li>
          <Link to={links.privacy}>Privacy Policy</Link>
        </li>
        <li>
          <Link to={links.airfleet} target="_blank">AirFleet</Link>
        </li>
      </ul>
    </div>
  </Footer>
));
