import React from 'react'
import styled from 'styled-components';
import { Link, withRouteData } from 'react-static';
import { Colors } from "../consts/styles";
//
import logo from '../svg/logo.svg';
import logoWhite from '../svg/logo-white.svg';

const Navbar = styled.nav`
  position: absolute;
  top: 0;
  right: 0;
  left: 0;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  padding-top: .5rem;
  
  .nav-links {
  
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: flex-start;
  
    img.brand {
      height: 45px;
    }
    
    a + a {
      margin-left: 3rem;
    }
    
    .nav-link{
      padding-top: .6rem;
      font-weight: 400;
      text-decoration: none;
      color: rgba(255, 255, 255, .6);
      text-transform: uppercase;
      font-size: .875rem;
      transition: .2s ease-in-out;
      
      &.active,
      &:hover{
        font-weight: 500;
        color: ${ Colors.WHITE };
      }
    }
    
    @media (max-width: 769px){
      justify-content: center;
    }
  }
`;

export default withRouteData(({navbar}) => (
  <Navbar>
    <div className="container">
      <div className="nav-links">
        <Link to="/">
          <img className="brand" src={ navbar === "white" ? logoWhite : logo } alt=""/>
        </Link>
      </div>
    </div>
  </Navbar>
));
