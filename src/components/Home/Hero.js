import React from 'react'
import styled from 'styled-components'
import { withSiteData } from 'react-static'
//
import { Colors } from "../../consts/styles";
import bgImg from '../../img/Home/top_fold.jpg';

const Hero = styled.section`
  height: 484px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  background: url("${bgImg}") center 30% no-repeat;
  background-size: 100%;
  background-blend-mode: luminosity;
  position: relative;
  
  &:before{
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-image: linear-gradient(132deg, ${Colors.GREEN} 0%, ${Colors.BLUE} 100%); //$green-blue-deep-gradient;
    opacity: .9;
  }
  
  .content{
    padding-top: 4rem;
    text-align: center;
    color: #fff;
    
    h2{
      padding-top: 1.25rem;
      max-width: 727px;
      margin: 0 auto;
    }
    
    a{
      margin: 3rem auto 0;
    }

    .cta-title {
      font-weight: bold;
    }

    .cta-container {
      display: flex;
      max-width: 610px;
      margin: auto;
      padding-bottom: 1em;
      .btn-cta {
        margin-top: 1.5em;

        @media (max-width: 655px){
          width: 30%;
        }
      }
    }
  }
  
  @media (max-width: 650px){
    height: 600px;
    background-size: cover;
  }
  
  @media (max-width: 321px){
    height: 650px;
  }
`;


export default withSiteData(({links}) => (
  <Hero>
    <div className="container">
      <div className="content">
        <h1>Focus on Coding, Nothing Else</h1>
        <h2>Join our network of global coders, work as much as you want from the comfort of your home</h2>
        <h2 className="cta-title">Apply Now!</h2>
        <div className="cta-container">
          <a href={links.applyWordpress} className="btn-cta">Wordpress Developer</a>
          <a href={links.applyFrontend} className="btn-cta">Frontend Developer</a>
          <a href={links.applyAnimator} className="btn-cta">Tech Artist</a>
        </div>
      </div>
    </div>
  </Hero>
));