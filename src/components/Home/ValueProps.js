import React from 'react';
import styled from 'styled-components';
import { Grid, Col, Row} from 'react-styled-flexboxgrid';
import {Colors} from "../../consts/styles";
//
import HowtoApply from './HowtoApply';
import flexIcon from "../../svg/Home/flex-icon.svg";
import arrowsIcon from "../../svg/Home/arrows-icon.svg";
import globeIcon from "../../svg/Home/globe-icon.svg";
import topLeftImg from '../../img/Home/mozaic-left-top@2x.png';
import bottomLeftImg from '../../img/Home/mozaic-left-bottom@2x.png';
import topRightImg from '../../img/Home/mozaic-right-top@2x.png';
import bottomRightImg from '../../img/Home/mozaic-right-bottom@2x.png';

const ValueProps = styled.section`
  display: flex;
  flex-direction: row;
  justify-content: center;
  background: linear-gradient(180deg, ${ Colors.LIGHT_GRAY } 0%, ${ Colors.WHITE } 100%);
  
  .content {
    padding-top: 3rem;
    text-align: center;
    
    .props-grid{
      margin: 4rem auto 3rem;
      padding: 0;
      
      .mozaic-grid{
        img{
          width: 100%;
          
          & + img{
            margin-top: .9rem;
          }
        }
        
        @media (max-width: 769px){
          display: none;
        }
      }
    }
  }
`;

const ValueProp = styled.div`
  text-align: left;
  margin-top: 3.5rem;
  
  h4 {
    display: flex;
    flex-direction: row;
    align-items: center;
    
    &:before {
      content: "";
      background: url("${props => props.icon}") 0 0 no-repeat;
      background-size: 100%;
      width: 3.125rem;
      height: 3.125rem;
      display: inline-block;
      margin-right: .65rem;
    }
  }
  
  p {
    font-size: 1rem;
    font-weight: 300;
    padding-left: 3.775rem;
    line-height: 1.5rem;
  }
  &:first-of-type{
    margin-top: 0;
  }
`;

export default () => (
  <ValueProps>
    <div className="container">
      <div className="content">
        <h3>Why Join the Clan</h3>
        <Grid fluid={true} className="props-grid">
          <Row>
            <Col lg={5} md={6} xs={12}>
              <ValueProp icon={flexIcon}>
                <h4>Flexible Workload</h4>
                <p>We take care of sourcing the work and there's plenty to go around. You can take a few tasks a week or work for 10+ hours a day.</p>
              </ValueProp>
              <ValueProp icon={arrowsIcon}>
                <h4>No Client Relations</h4>
                <p>You'll only need to work with our technical project managers. They take care of scoping the work, environment configuration and client communication.</p>
              </ValueProp>
              <ValueProp icon={globeIcon}>
                <h4>Work Anywhere</h4>
                <p>Work from your house, a coffee-shop, or the beach. As long as you have a stable internet connection you can work from anywhere in the world.</p>
              </ValueProp>
            </Col>
            <Col lg={3} lgOffset={1} md={3}>
              <div className="mozaic-grid">
                <img src={topLeftImg} alt=""/>
                <img src={bottomLeftImg} alt=""/>
              </div>
            </Col>
            <Col lg={3} md={3}>
              <div className="mozaic-grid">
                <img src={topRightImg} alt=""/>
                <img src={bottomRightImg} alt=""/>
              </div>
            </Col>
          </Row>
        </Grid>
        <HowtoApply />
      </div>
    </div>
  </ValueProps>
)