import React from 'react';
import styled from 'styled-components';
import { Grid, Col, Row} from 'react-styled-flexboxgrid';
import {Colors} from "../../consts/styles";
import { withSiteData } from 'react-static'
//
import planeIcon from '../../svg/Home/airplane-icon.svg';
import magnifyIcon from '../../svg/Home/mag-icon.svg';
import flaskIcon from '../../svg/Home/flask-icon.svg';
import checkIcon from '../../svg/Home/checkmark-icon.svg';

const HowtoApply = styled.section`
  background-color: ${Colors.WHITE};
  border-radius: .625rem;
  box-shadow: 0 2px 5px rgba(54, 54, 54, .7);
  padding: 2.5rem 1.875rem;
  text-align: center;
  margin-bottom: -4rem;
  
  .cta-container {
    display: flex;
    max-width: 610px;
    margin: auto;
    .btn-cta {
      @media (max-width: 655px) {
        width: 30%;
      }
    }
    
  }

  .apply-grid{
    margin-top: 3rem;
    padding: 0;

    img{
      width: 4.375rem;
      height: 4.375rem;
      margin-bottom: 1.25rem;
      
      @media (max-width: 415px){
        margin-bottom: 1rem;
      }
    }
    
    h5{
      margin-bottom: .5rem;
      
      @media (max-width: 415px){
        margin-bottom: 2.5rem;
      }
    }
    
    p{
      font-weight: 400;
      font-size: .9rem;
      
      @media (max-width: 769px){
        padding-bottom: 2rem;
      }
      
      @media (max-width: 415px){
        display: none;
      }
    }
  }
  
  hr{
    border-top: 0;
    border-color: #eee;
    margin: 2.5rem 0;
    
    @media (max-width: 415px){
      margin-top: 0;
    }
  }
  
  a{
    margin: 1rem auto 0;
  }
`;

export default withSiteData(({links}) => (
  <HowtoApply>
    <h3>How to Apply</h3>
    <Grid fluid={true} className="apply-grid">
      <Row>
        <Col lg={3} md={3} sm={6} xs={12}>
          <img src={planeIcon} alt=""/>
          <h5>Submit your Details</h5>
          <p>Be sure to include your CV and professional profiles.</p>
        </Col>
        <Col lg={3} md={3} sm={6} xs={12}>
          <img src={magnifyIcon} alt=""/>
          <h5>Our Team Reviews</h5>
          <p>Our team will reach out to suitable applications.</p>
        </Col>
        <Col lg={3} md={3} sm={6} xs={12}>
          <img src={flaskIcon} alt=""/>
          <h5>Join our Trial</h5>
          <p>During our 30 day trial period we'll evaluate our mutual fit.</p>
        </Col>
        <Col lg={3} md={3} sm={6} xs={12}>
          <img src={checkIcon} alt=""/>
          <h5>Become a Member</h5>
          <p>Enjoy the full benefits of the CodersClan community.</p>
        </Col>
      </Row>
    </Grid>
    <hr/>
    <h3>Ready to Become a Member</h3>
    <div className="cta-container">
      <a href={links.applyWordpress} className="btn-cta">Wordpress Developer</a>
      <a href={links.applyFrontend} className="btn-cta">Frontend Developer</a>
      <a href={links.applyAnimator} className="btn-cta">Animator</a>
    </div>
  </HowtoApply>
));
