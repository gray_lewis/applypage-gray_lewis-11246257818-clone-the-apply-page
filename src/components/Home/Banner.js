import React from 'react';
import styled, { keyframes } from 'styled-components';
import { Colors } from "../../consts/styles";
import { SiteData } from 'react-static';
//
const slideIn = keyframes`
  from  {
    top: -4rem;
  }
  
  to {
    top: 0;
  }
`;

const slideOut = keyframes`
  from  {
    top: 0;
    opacity: 1;
  }
  
  to {
    top: -4rem;
    opacity: 0;
  }
`;

const BannerSection = styled.section`
  position: fixed;
  top: -4rem;
  left: 0;
  right: 0;
  height: 4rem;
  background-color: #333;
  animation: ${slideIn} .5s ease-in forwards;
  animation-delay: 1s;
  text-align: center;
  z-index: 10;
  
  display: flex;
  align-items: center;
  justify-content: center;
  
  p{
    color: ${ Colors.WHITE } !important;
    font-weight: bold;
    
    a{
      color: ${ Colors.GREEN };
    }
  }
  
  .close-icon{
    color: rgba(255, 255, 255, 0.5);
    position: absolute;
    right: 1rem;
    cursor: pointer;
    transition: 0.1s ease-in-out;
    
    &:hover{
      color: ${ Colors.WHITE };
    }
  }
  
  &.close{
    animation: ${slideOut} .5s ease-in forwards;
  }
  
  @media (max-width: 415px){
    display: none;
  }
`;

export default class Banner extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      show: true
    };

    this.closeBanner = this.closeBanner.bind(this);
  }

  closeBanner() {
    this.setState({show: false});
  }

  render() {
    return (
      <SiteData>
        {({ links }) => (
          <BannerSection className={this.state.show ? "" : "close"}>
            <div className="close-icon" onClick={this.closeBanner}>✕</div>
            <p>Looking to start a project? 👉 <a href={links.airfleet} target="_blank">Check out AirFleet</a></p>
          </BannerSection>
        )}
      </SiteData>
    );
  }
}