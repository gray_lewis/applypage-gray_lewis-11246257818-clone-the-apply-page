import React from 'react';
import styled from 'styled-components';
import {Colors} from "../../consts/styles";
import {withSiteData} from 'react-static';
import Slider from "react-slick"
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"
//
import samImg from '../../img/Home/Sam@2x.jpg';
import nunoImg from '../../img/Home/Nuno@2x.jpg';
import bishoyImg from '../../img/Home/Bishoy@2x.png';
import sliderRight from '../../svg/Home/slider-right.svg';
import sliderLeft from '../../svg/Home/slider-left.svg';

const Testimonials = styled.section`
  background-color: ${ Colors.LIGHT_GRAY };
  padding: 3rem 0;
  display: flex;
  flex-direction: row;
  justify-content: center;
  
  h3{
    text-align: center;
  }
  
  .testimonials-carousel{
    
    .slick-slider {
    
      position: relative;
      margin-top: 2.5rem;
  
      button.slick-arrow {
        text-indent: 100%;
        white-space: nowrap;
        overflow: hidden;
        outline: none !important;

        background-color: transparent;
        border: none;
        box-shadow: none;
        padding: 0;
        width: 2.5rem;
        height: 1.5rem;

        position: absolute;
        top: calc(50% - .75rem);
        z-index: 9999;
        transition: 0.1s ease-in-out;

        &:hover {
          filter: brightness(80%);
        }

        &.slick-prev {
          background: url("${sliderLeft}") center center no-repeat;
          background-size: contain;
          left: 0;
        }

        &.slick-next {
          background: url("${sliderRight}") center center no-repeat;
          background-size: contain;
          right: 0;
        }
      }
      
      .slide-container{
        outline: none !important;
        .slide{
          outline: none !important;
          padding: 2rem 5rem; 
          background-color: ${ Colors.LIGHT_GRAY };
  
          display: flex;
          flex-direction: row;
          align-items: center;
          justify-content: center;
          
          @media (max-width: 415px){
            padding: 1rem;
            flex-direction: column;
          }
          
          @media (max-width: 321px){
            padding: 1.5rem;
          }
          
          .img-container{
            img {
              width: 200px;
              height: 200px;
              border-radius: 50%;
            }
            
            @media (max-width: 415px){
              margin-bottom: 2rem;
            }
          }
  
          .content {
  
            text-align: left;
            margin-left: 1.875rem;
            
            @media (max-width: 415px){
              margin-left: 0;
              text-align: center;
            }
  
            .title {
              font-size: 1.375rem;
              font-weight: 500;
              color: #777;
              margin-bottom: .25rem;
            }
  
            .subtitle {
              font-size: 1.15rem;
              font-weight: 400;
              color: #999;
              margin-bottom: .625rem;
            }
  
            .description {
              font-size: 1rem;
              font-weight: 400;
              color: ${ Colors.LIGHT_BLACK };
            }
          }
        }
      }
    }
  }
  
  hr{
    border-top: 0;
    border-color: #ddd;
    margin: 2.5rem 0 3.5rem;
    
    @media (max-width: 415px){
      margin-top: 1rem;
    }
  }
  
  .bottom-cta{
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    
    @media (max-width: 769px){
      flex-direction: column;
    }
    
    h3{
      margin-right: 1.5rem;
      text-align: left;
      
      @media (max-width: 769px){
        text-align: center;
        margin: 0 0 2rem 0;
      }
    }
  }
`;

export default withSiteData(({links}) => (
  <Testimonials>
    <div className="container">
      <h3>What Our Coders Say About Us</h3>
      <div className="testimonials-carousel">
        <Slider autoplay={true} autoplaySpeed={5000}>
          <div className="slide-container">
            <div className="slide">
              <div className="img-container">
                <img src={samImg} alt=""/>
              </div>
              <div className="content">
                <p className="title">
                  Sam Benson, UK
                </p>
                <p className="subtitle">
                  2 years with CodersClan
                </p>
                <p className="description">
                  "CodersClan allowed me to quit my day job and still support my family. It's been an amazing experience with these guys."
                </p>
              </div>
            </div>
          </div>
          <div className="slide-container">
            <div className="slide">
              <div className="img-container">
                <img src={nunoImg} alt=""/>
              </div>
              <div className="content">
                <p className="title">
                  Nuno Freitas, Portugal
                </p>
                <p className="subtitle">
                  2 years with CodersClan
                </p>
                <p className="description">
                  "CodersClan gives me the freedom to set my own schedule and choose the projects I find most interesting. The project managers are really helpful in handling everything for me and providing me all the tools that I need so I can just focus on developing software."
                </p>
              </div>
            </div>
          </div>
          <div className="slide-container">
            <div className="slide">
              <div className="img-container">
                <img src={bishoyImg} alt=""/>
              </div>
              <div className="content">
                <p className="title">
                  Bishoy Awad, Egypt
                </p>
                <p className="subtitle">
                  2.5 years with CodersClan
                </p>
                <p className="description">
                  "With CodersClan I gained access to more projects that I wouldn't usually have access to. The fact they manage everything makes my life so easy."
                </p>
              </div>
            </div>
          </div>
        </Slider>
      </div>
      <hr/>
      <div className="bottom-cta">
        <h3>Become a Part of the CodersClan Network</h3>
        <a href={links.apply} className="btn-cta">Apply Now</a>
      </div>
    </div>
  </Testimonials>
));