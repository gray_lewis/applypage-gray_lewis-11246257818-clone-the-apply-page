import React from 'react';
import styled from 'styled-components';
import { Grid, Col, Row} from 'react-styled-flexboxgrid';
import {Colors} from "../../consts/styles";
//
import flaskInactive from '../../svg/Home/flask-faq-inactive2.svg';
import flaskHover from '../../svg/Home/flask-faq-inactive1.svg';
import flaskActive from '../../svg/Home/flask-faq-active.svg';
import cogInactive from '../../svg/Home/cog-faq-inactive2.svg';
import cogHover from '../../svg/Home/cog-faq-inactive1.svg';
import cogActive from '../../svg/Home/cog-faq-active.svg';
import dollarInactive from '../../svg/Home/dollar-faq-inactive2.svg';
import dollarHover from '../../svg/Home/dollar-faq-inactive1.svg';
import dollarActive from '../../svg/Home/dollar-faq-active.svg';

const FAQSection = styled.section`
  display: flex;
  flex-direction: row;
  justify-content: center;
  padding: 7rem 0 3rem;
  background: linear-gradient(-47deg, ${ Colors.BLUE } 0%, rgba(9,24,63,0.75) 100%);
  
  .content{
    h3{
      text-align: center;
      color: ${ Colors.WHITE };
    }
    
    .faq-grid{
      margin-top: 3rem;
      padding: 0;
      
      .categories{
        position: relative;
        
        .category{
          padding: 1rem 0 1rem 1rem;
          display: flex;
          flex-direction: row;
          align-items: center;
          cursor: pointer;
                    
          h5{
            color: rgba(255, 255, 255, .6);
            font-weight: 400;
            transition: 0.2s ease-in-out;
            font-size: 1.25rem;
          }
          
          .cat-icon{
            background-position: center !important;
            background-repeat: no-repeat !important;
            width: 1.75rem;
            height: 1.75rem;
            transition: 0.2s ease-in-out;
            margin-right: .35rem;
            
            &.trial{
              background: url("${ flaskInactive }");
            }
            
            &.general{
              background: url("${ cogInactive }");
            }
            
            &.payments{
              background: url("${ dollarInactive }");
            }
          }
          
          &:hover{
            h5{
              color: ${ Colors.WHITE };
            }
          
            .cat-icon{
              &.trial{
                background: url("${ flaskHover }");
              }
              
              &.general{
                background: url("${ cogHover }");
              }
              
              &.payments{
                background: url("${ dollarHover }");
              }
            }
          }
          
          &.active{
            background: linear-gradient(180deg, ${ Colors.WHITE } 0%, #DDD 100%);
            box-shadow: 2px 2px 4px rgba(51, 51, 51, 0.5);
            border-radius: .5rem;
          
             h5{
              color:#555 !important;
              font-weight: 500;
            }
          
            .cat-icon{
              &.trial{
                background: url("${ flaskActive }") center no-repeat !important;
              }
              
              &.general{
                background: url("${ cogActive }") center no-repeat !important;
              }
              
              &.payments{
                background: url("${ dollarActive }") center no-repeat !important;
              }
            }
          }
        }
      }
      
      .questions-box{
        background: linear-gradient(180deg, ${ Colors.WHITE } 0%, #DDD 100%);
        height: 345px;
        box-shadow: 2px 2px 4px rgba(51, 51, 51, 0.5);
        border-radius: .625rem;
        padding: 2rem;
        overflow: scroll;
        
        @media (max-width: 769px){
          margin-top: 1rem;
        }
        
        .questions{
          display: none;
          
          &.active{
            display: block;
          }
                  
          .question{
            color: ${ Colors.LIGHT_BLACK };
            
            h4{
              margin-bottom: 1rem;
            }
            
            ul{
              list-style: disc;
              padding-left: 1.5rem;
              
              li{
                margin-bottom: .5rem;
                line-height: 1.5rem;
              }
            }
            
            & + .question{
              margin-top: 3.5rem;
            }
          }
        }
      }
    }
  }
`;

export default class FAQ extends React.Component {
  constructor(props){
    super(props);

    this.state = { active: "trial" };

    this.handleCategoryClick = this.handleCategoryClick.bind(this);
  }

  handleCategoryClick(event) {
    this.setState( { active: event.currentTarget.id } );
  }

  render() {

    const isActive = category => category === this.state.active ? 'active' : "";

    return (
      <FAQSection>
        <div className="container">
          <div className="content">
            <h3>Frequently Asked Questions</h3>
            <Grid fluid={true} className="faq-grid">
              <Row>
                <Col lg={3} md={4} xs={12}>
                  <div className="categories">
                    <div id="trial" className={`category ${isActive('trial')}`} onClick={this.handleCategoryClick}>
                      <div className="cat-icon trial"></div>
                      <h5>Trial Process</h5>
                    </div>
                    <div id="general" className={`category ${isActive('general')}`} onClick={this.handleCategoryClick}>
                      <div className="cat-icon general"></div>
                      <h5>General</h5>
                    </div>
                    <div id="payments" className={`category ${isActive('payments')}`} onClick={this.handleCategoryClick}>
                      <div className="cat-icon payments"></div>
                      <h5>Payments</h5>
                    </div>
                  </div>
                </Col>
                <Col lg={9} md={8} xs={12}>
                  <div className="questions-box">
                    <div className={`questions ${isActive('trial')}`}>
                      <div className="question">
                        <h4>How does the trial process work?</h4>
                        <ul>
                          <li>The trial period duration is 30 days, after that it's up to both parties to decide whether they'd like to move forward.</li>
                          <li>During the trial period, CodersClan project managers will send small tasks your way that you may elect to tackle at your own discretion</li>
                          <li>For the trial period, all tasks are fix priced - that means that we offer the task for a fixed cost and you may elect on whether you would like to complete it or not. Any taken task must be completed to our satisfaction and we can work with you and provide feedback to make sure it meets our requirements.</li>
                        </ul>
                      </div>
                      <div className="question">
                        <h4>What are the terms of work once I pass the trial?</h4>
                        <p>Once the trial is completed, and we decide to keep working together, you'll be eligible for hourly work on a $30/hour basis.</p>
                      </div>
                    </div>
                    <div className={`questions ${isActive('general')}`}>
                      <div className="question">
                        <h4>How do I communicate with the PM and get tasks?</h4>
                        <p>
                          We use Slack for most of our communication inside the community - there you will get to know the PM's and other coders you will collaborate with. We manage our available tasks on Trello where you can see at all times the available work and assign yourself to what you find interesting.
                        </p>
                      </div>
                      <div className="question">
                        <h4>What are your work hours?</h4>
                        <p>Our PM's are usually available between business hours (10am - 6pm) depending on their timezone (some are based in Europe others in the US). You can work whenever is most comfortable for you, just make sure to have some overlapping time your PM for questions and information.</p>
                      </div>
                    </div>
                    <div className={`questions ${isActive('payments')}`}>
                      <div className="question">
                        <h4>When do you send payouts?</h4>
                        <p>
                          Payouts are sent on a NET + 10 basis. It means that for every work completed and approved in a specific month you get paid for it on 10th of the next month. E.g. if for work completed and approved during September you'll get paid on October 10th.
                        </p>
                      </div>
                      <div className="question">
                        <h4>What payout methods do you support?</h4>
                        <p>We provide payouts through Payoneer which is a global payment service. Once you are accepted to CodersClan, you will be provided with a special link with which you could associate a new or existing Payoneer account with our payments account. Make sure you are willing to comply with Payoneer terms and that Payoneer is supported in your country before you sign up to CodersClan.</p>
                      </div>
                    </div>
                  </div>
                </Col>
              </Row>
            </Grid>
          </div>
        </div>
      </FAQSection>
    );
  }
}