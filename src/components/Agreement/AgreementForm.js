import React from 'react'
import styled from 'styled-components'
import {Colors} from "../../consts/styles";
import { Grid, Row, Col } from 'react-styled-flexboxgrid';
import { withSiteData, withRouter } from 'react-static';
//

const AgreementFormSection = styled.section`

  padding: 2rem 0 0;

  form{
    
    color: ${ Colors.LIGHT_BLACK };
    
    input[type="text"],
    input[type="email"]{
      box-sizing: border-box;
      width: 100%;
      margin-bottom: 1rem;
      border: none;
      border-radius: .25rem;
      background-color: ${ Colors.WHITE };
      box-shadow: 0 2px 4px rgba(54, 54, 54, .5);
      font-size: 1rem;
      padding: 1rem;
      outline: none;
      font-weight: 500;
      transition: 0.2s ease-in-out;

      &::-webkit-input-placeholder{
        color: rgba(140, 140, 140, 0.5);
        font-size: .9rem;
      }
      
      &:focus{
        box-shadow: 0 2px 10px rgba(54, 54, 54, .7);
      }
        
    }
    
    .checkbox-label{
      
      display: flex;
      flex-direction: row;
      align-items: center;
      justify-content: center;
      margin: 1rem 0 2rem;
      cursor: pointer;
    
      input{
        display: inline-block;
        margin-right: 1rem;
        cursor: pointer;
      }
    }
  
    button{
      margin: 0 auto;
      cursor: pointer;
      
      &:disabled{
        cursor: not-allowed;
        filter: opacity(.5) !important;
      }
    }
  }
`;

function encode(data) {
  return Object.keys(data)
    .map(key => encodeURIComponent(key) + "=" + encodeURIComponent(data[key]))
    .join("&");
}

class AgreementForm extends React.Component{
  constructor(props){
    super(props);
    this.state = { loading: false, type: 'wordpress' };
  }

  componentDidMount() {
    const { match } = this.props;
    const params = match.params ? match.params[0] : false;
    if(params){
      const split = params.split('/');
      const type = split[split.length-1];
      this.setState({ type })
    }
  }

  handleChange = e => {
    this.setState({[e.target.name]: e.target.value});
  };

  handleSubmit = e => {
    const { links, history } = this.props;
    e.preventDefault();

    this.setState( { loading: true } );

    fetch(links.agreementWebhook, {
      method: "POST",
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      body: encode({ "form-name": this.state.type, ...this.state })
    })
      .then(() => history.push(links.agreementSuccess))
      .catch(err => {
        console.log(err);
        this.setState( { loading: false } );
      });
  };

  render() {
    const { links } = this.props;

    return(
      <AgreementFormSection>
        <div className="form-container">
          <form name={this.state.type} method="post" action={links.agreementSuccess} onSubmit={this.handleSubmit}>
            <Grid fluid={true} style={{padding: 0}}>
              <Row>
                <Col lg={3} lgOffset={3} md={6} sm={12} xs={12}>
                  <input type="text" name="full_name" placeholder="Full Name" required onChange={this.handleChange} />
                </Col>
                <Col lg={3} md={6} sm={12} xs={12}>
                  <input type="email" name="email" placeholder="Email" required onChange={this.handleChange}/>
                </Col>
              </Row>
              <Row>
                <Col lg={3} lgOffset={3} md={6} sm={12} xs={12}>
                  <input type="text" name="address" placeholder="Street Address" onChange={this.handleChange}/>
                </Col>
                <Col lg={3} md={6} sm={12} xs={12}>
                  <input type="text" name="city" placeholder="City" onChange={this.handleChange}/>
                </Col>
              </Row>
              <Row>
                <Col lg={3} lgOffset={3} md={6} sm={12} xs={12}>
                  <input type="text" name="country" placeholder="Country" onChange={this.handleChange}/>
                </Col>
                <Col lg={3} md={6} sm={12} xs={12}>
                  <input type="text" name="state_id" placeholder="Passport ID/Resident ID/SSN" onChange={this.handleChange}/>
                </Col>
              </Row>
              <Row>
                <Col lg={6} lgOffset={3} md={6} sm={12} xs={12}>
                  <input type="email" name="gitlab" placeholder="GitLab Email" required onChange={this.handleChange}/>
                </Col>
              </Row>
            </Grid>
            <div>
              <label className="checkbox-label">
                <input type="checkbox" name="agree" value="agree" required onChange={this.handleChange}/>I agree to all terms of the above agreement
              </label>
            </div>
            <button type="submit" className="btn-cta" disabled={this.state.loading}>
              { this.state.loading ? "Just a sec" : "Submit" }
            </button>
          </form>
        </div>
      </AgreementFormSection>
    );
  }
}

export default withRouter(withSiteData(AgreementForm));
