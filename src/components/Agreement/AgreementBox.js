import React from 'react'
import styled from 'styled-components'
import {Colors} from "../../consts/styles";
//

const AgreementBox = styled.section`
  background-color: ${ Colors.WHITE };
  border-radius: 8px;
  height: 415px;
  overflow: scroll;
  box-shadow: 0 2px 4px rgba(54, 54, 54, .5);
  border: 1.25rem solid ${ Colors.WHITE };
  margin-top: 1.75rem;
  padding: 0.625rem 1.25rem;
  resize: vertical;
  
  strong{
    font-weight: bold;
  }
  
  p{
      padding-bottom: 1rem;
    }
    
    ol{
      list-style: decimal;
      padding-left: 2rem;
      li{
        margin-bottom: 1rem;
        line-height: 1.5rem;
      }
    }
    
  .text-center{
    text-align: center;
  }
  
  .agreement-content{
    color: ${ Colors.LIGHT_BLACK };
    margin-bottom: 2.5rem;
    
    .small-indent{
      margin-left: 5px;
    }
  }
  
  .exhibit-a{
      border-top: 1px solid #eee;
      padding-top: 2.5rem;
      margin-bottom: 2.5rem;
    }
  
    .exhibit-b{
      border-top: 1px solid #eee;
      padding-top: 2.5rem;
  
      .indented{
        text-indent: 3.125rem;
      }
    }  
`;

export default () => (
  <AgreementBox>
    <div className="agreement-content">
      <p><strong>Re:</strong><strong>Engagement Offer</strong></p>
      <p>Further to our discussions, we are pleased to offer you (the "<strong>Service Provider</strong>" or "<strong>you</strong>") 
      to engage with CodersClan Inc. (the "<strong>Company</strong>" or "<strong>we</strong>"), as a service provider
       to the Company, in accordance with the following terms (the "<strong>Agreement</strong>"):<br/><br/>
       
       We may modify this Agreement from time to time. We will make commercially
        reasonable efforts to provide you with written notice, as required by this
        Agreement, of any material changes to this Agreement. Any such changes will
        come into effect automatically within 30 days from our provision of written notice
        thereof. If you object to any such changes, you may provide written notice of
        termination, in which case such changes will not come into effect for you, and
        this Agreement will be terminated upon the conclusion of the aforementioned 30
        day period.
       
       </p>
             <ol><li><u>Services</u>. During the Term (as defined below), you agree to render to the Company,
             on a non-exclusive basis, certain services and deliverables with respect to the business of the Company as currently 
             being conducted and as proposed to be conducted (the "<strong>Company's Business</strong>"), 
             primarily including the services and deliverables set forth in <u>Exhibit A</u> hereto, as well as such other 
             related services or deliverables reasonably requested by the Company in furtherance thereof, whether or not 
             specified in <u>Exhibit A</u> (respectively, "<strong>Services</strong>" and "<strong>Deliverables</strong>") 
             based on the scope of Services set forth in <u>Exhibit A</u>. You agree to adhere to the time schedule requested by 
             the Company for the fulfillment of the Services and the Deliverables. All Services and Deliverables hereunder shall 
             be subject to the Company's inspection and testing at all reasonable times and places.</li><li><u>Term of Engagement</u>. 
             Your engagement with the Company shall be deemed to have commenced on {(new Date()).toDateString()}&nbsp;
      (the "<strong>Commencement Date</strong>"), and may be terminated at any time by either party granting the other 
      party a fourteen (14) day prior written notice of such termination, provided that the Service Provider may not 
      erminate this Agreement during an active engagement on an ongoing project or account with a client of the 
      Company (a "<strong>Project</strong>") unless he provides advance written notice of at least thirty (30) 
      days &nbsp;(the "<strong>Term</strong>"). Upon receipt of a termination notice and except as otherwise directed 
      by the Company, the Service Provider shall immediately: (i) stop work on the Services and the Deliverables; (ii) 
      take all action necessary or as the Company may direct in writing to minimize the cost of the termination; 
      and (iii) notify the Company of the status of all work in progress. The Company shall not be liable for any 
      loss or damage suffered by Service Provider as a result of any termination.</li><li><u>Consideration; 
        Reimbursement of Expenses</u>.<ol><li>In consideration for the Services and the Deliverables, you shall be 
          entitled to receive the consideration set forth in <u className="small-indent">Exhibit A</u> hereto 
          (the "<strong>Consideration</strong>"). In order to receive the Consideration, you must satisfy the 
          requirements set forth in this Agreement and Exhibit A, including the receipt of any required approvals 
          from Company. Company shall not be liable to make payment of any Consideration in respect of Services 
          performed or Deliverables provided not in compliance with this Agreement and Exhibit A. </li><li><span></span>
          The Consideration has been determined as fair remuneration for the Services and the Deliverables. The Company 
          shall not be liable for any additional payments or contributions to the Service Provider or any of its 
          personnel in connection with the Services and the Deliverables. Service Provider expressly
          waives all claims in respect of any consideration other than as expressly set
          forth in this Agreement, including in respect of any assignment and licenses
          made by Service Provider in respect of intellectual property.</li><li>You shall bear all the expenses 
            incurred by you in connection with the provision of the Services and the Deliverables, including all 
            taxes and costs incurred in connection with your compliance with this Agreement. Without derogating 
            from the generality of the foregoing, you may at times be reimbursed for expenses incurred by you, 
            subject to the prior written approval of such expenses by the Company.</li></ol></li><li><u>
              Service Provider Representations and Undertakings</u>.<ol><li>You hereby represent and warrant 
                that (i) your execution, delivery and performance of this Agreement will not violate any provision 
                of law or agreement applicable to you; (ii) the execution and delivery of this Agreement and 
                the fulfillment of its terms: (a) will not constitute a default under or conflict with any 
                agreement or other instrument to which you are a party or by which you are bound, 
                including but not limited your obligations with respect to disclosure of and assignment of 
                Inventions to the Company, as set forth in<u className="small-indent">Exhibit B</u> hereto; 
                and (b) do not require the consent of any person or entity; (iii) with respect to any past 
                engagement of the Service Provider with third parties and with respect to any permitted 
                engagement of the Service Provider with any third party during the term of your engagement 
                with the Company, you hereby represent, warrant and undertake that: (a) your engagement with the 
                Company is not now, and will not in the future be, in breach of any of your undertakings toward 
                such third parties, including, without limitation, any non-competition or confidentiality 
                undertakings nor will any Services, Deliverables or other work you perform for the Company grant 
                any third party any claim to or title, ownership or any other right in any Property Information or 
                Company Inventions (each as defined in Exhibit B hereto); and (b) you will not disclose to the 
                Company, nor use, in provision of any services to the Company, any proprietary or confidential 
                information belonging to any third party; and (iv) as of the Commencement Date, the Company has 
                no outstanding obligations to the Service Provider for any services which may have been provided 
                by the Service Provider at any time prior thereto (in any capacity), and the Service Provider has 
                no claims towards the Company (or its officers, directors, shareholders and affiliates), and 
                forever discharges the Company from any claims it may have in connection therewith.</li><li><span></span>
                By executing this Agreement, you confirm and agree to the provisions of the Company's Proprietary 
                Information, Confidentiality and Non-Competition Agreement attached as
                <u className="small-indent">Exhibit B</u> hereto, the provisions of which shall 
                survive the termination of this Agreement for any reason.</li></ol></li><li><u>
                  Independent Contractor Relations</u>.<ol><li>Service Provider warrants and represents that (i) 
                    Service Provider is an independent contractor, owner of Service Provider's own business; and (ii) 
                    the Company will withhold at source from all payments made to Service Provider, the necessary tax 
                    it is required to withhold pursuant to applicable law. The relationship of Service Provider with 
                    the Company is that of an independent contractor and neither Service Provider, its agents, 
                    representatives nor employees shall be considered employees of the Company.</li><li><span></span>
                    If the Service Provider or any third party should claim that there existed an employer-employee 
                    relationship between the Company and the Service Provider and/or any of its personnel, resulting 
                    in the Company being obligated to pay any additional amounts over and above the Consideration, in 
                    connection therewith then in such an event any consideration or amount paid to Service Provider 
                    in accordance with the terms hereof, in excess of a gross amount equal to 50% of the fees 
                    (which shall be deemed a gross monthly salary including all social benefits payable by law) 
                    paid under this Agreement herein, shall be deemed a loan provided by the Company to the Service 
                    Provider (the "<strong>Loan</strong>"). The Loan shall be linked to the Consumer Price Index 
                    and incur an annually compounded interest at an annual a rate of 10% from the date it was 
                    forwarded to the Service Provider and until repayment in full. The Loan plus any linkage 
                    differentials and interest accrued thereon shall be due and repayable by the Service 
                    Provider immediately upon the first demand or claim made by Service Provider or any 
                    third party to the effect that there existed and/or exists an employer-employee relationship 
                    between the Company and the Service Provider.</li><li><span></span>Service Provider will not, 
                    without the Company's prior written approval, make any binding representations about the Company, 
                    nor shall it act as the Company's agent, nor shall it have any authority whatsoever to 
                    propose or accept, in the name and on behalf of the Company any representation, undertaking, 
                    guarantee, or any other kind of an obligation.</li></ol></li><li><u>Miscellaneous</u>. 
                    The laws of the State of New York shall apply to this Agreement and the sole and 
                    exclusive place of jurisdiction in any matter arising out of or in connection with this 
                    Agreement shall be the competent courts in the State of New York. Notwithstanding the 
                    foregoing, either party may seek an interim injunction or other interim equitable 
                    relief in any court of competent jurisdiction. All waivers must be in writing. 
                    In the event that it shall be determined under any applicable law that a certain 
                    provision set forth in this Agreement is invalid or unenforceable, such determination 
                    shall not affect the remaining provisions of this Agreement unless the business 
                    purpose of this Agreement is substantially frustrated thereby. The Service Provider 
                    shall not be entitled to assign Service Provider's rights or undertakings hereunder 
                    to any third party without obtaining the Company's prior written consent. 
                    Company may assign this Agreement to third parties.
                    This Agreement and the Exhibits attached hereto constitute the entire understanding 
                    and agreement between the parties hereto, supersedes any and all prior discussions, 
                    agreements and correspondence with regard to the subject matter hereof, 
                    and may not be amended, modified or supplemented in any respect, except as set forth herein, by a subsequent agreement between the parties, or by
                    a subsequent writing executed by both parties hereto. Any notice that Company
                    is required to provide under this Agreement, or which Company may provide
                    pursuant to applicable law, may be provided by Company by written or electronic
                    notice to the contact information in Service Provider&#39;s account, including via
                    email.</li>
      </ol>
    </div>
    <div className="exhibit-a">
      <p className="text-center"><strong><u>Exhibit A</u></strong></p><p><strong><u>Services</u></strong><strong><u>, 
        Deliverables</u></strong><strong><u> and Consideration</u></strong></p><p><u>Description of the Services</u>
        <br/>Development and consultation services for CodersClan and its clients.</p><p><u>Description of the 
          Deliverables</u><br/>Certain deliverables to be discussed and agreed from time to time, delivery of 
          which shall be subject to the verification and approval of the Company, at its sole discretion and will 
          be based on the internal procedures of the Company at the time that the deliverables were provided to 
          the Company.</p><p><u>Scope</u><br/>The scope of work shall be coordinated in advance by mutual agreement 
          with the Company. All hours shall be tracked and reported to the Company on an ongoing basis. Prior to 
          commencing any Services, Service Provider must prepare a written time estimate of the time required to 
          provide such Services, submit such estimate to Company, and receive Company’s written approval of such 
          estimate. In addition, Service Provider must obtain Company’s approval prior to performing any Services 
          in excess of such estimate. Company shall have no obligation to make payment of any Services performed 
          or Deliverables provided either (a) prior to receipt of Company’s written approval of an estimate or (b) 
          in excess of any estimate if such excess has not been approved in writing by Company.</p>
          <p><u>Acceptance:</u><br/>
          Within 30 days of your delivery of the applicable Deliverables, Company (or its
          client) may examine the Deliverables to ensure they are of at least industry
          standard quality and satisfy the requirements of this agreement and any agreed
          specifications and requirements. Company (or its client) may reject any
          deliverables that do not satisfy the foregoing. Company’s (or its client’s)
          acceptance of any Deliverables should not be interpreted as a waiver of any
          claims regarding the quality of any Deliverables or the satisfaction of any
          warranties in this Agreement.</p>
          
          <p><u>Consideration</u><br/>In consideration of the full and complete execution of the 
          Services and any accepted Deliverables, as specified in the Agreement to which this Exhibit is attached 
          (the "<strong>Agreement</strong>"), you shall receive:</p><p>Compensation in the gross amount of 
            either (i)&nbsp;$18&nbsp;USD (Eighteen US Dollars) per hour for "Front-End" coders, 
            (ii)&nbsp;$30&nbsp;USD (Thirty US Dollars) per hour for Full-Stack WordPress coders, 
            (iii) other hourly amount pending the written approval of the COO or CEO of CodersClan, or 
            (iiii)&nbsp;a fixed dollar amount&nbsp;as shall be determined by the Company in its sole discretion 
            (the "<strong>Cash Compensation</strong>"). The Company shall notify the Service Provider at 
            the commencement of each Project as to the selected form of Cash Compensation for the specific 
            Project. The Cash Compensation shall be paid on a monthly basis, subject to your continued engagement 
            by the Company at the end of each month, within 10&nbsp;days of the end of the preceding month, 
            and against a valid invoice furnished by you to the Company.</p><p>It is hereby clarified that Cash
              Compensation payments shall be made to a Payoneer account, the details of which shall be provided 
              by the Service Provider to the Company in writing at a later date.
              Company has no obligation to make payment for any Deliverables rejected by
              Company or its client.</p>
    </div>
    <div className="exhibit-b">
      <p className="text-center"><strong><u>Exhibit B</u></strong></p><p><strong><i><u>Proprietary Information, 
        Confidentiality and Non-Competition Agreement</u></i></strong></p><p>1.
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong><u>General</u></strong></p><p>
            All the capitalized terms herein shall have the meanings ascribed to them in the Engagement 
            Offer to which this Exhibit is attached (the "<strong>Agreement</strong>"). For purposes of 
            any undertaking of Service Provider toward the Company, the term "Company" shall include any 
            parent company and all subsidiaries and affiliates of the Company.</p><p>Service Provider's 
              obligations and representations and the Company's rights under this Exhibit shall apply as of the 
              first date of Service Provider's engagement and association with the Company, even if such date precedes 
              the Commencement Date.</p><p>2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong><u>Confidentiality; 
                Proprietary Information</u></strong></p><p className="indented">2.1
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "<strong>Proprietary Information</strong>" means confidential and
                 proprietary information concerning the Company's Business and financial activities of the Company, 
                 including patents, patent applications, trademarks, copyrights and other intellectual property, 
                 and information relating to the same, technologies and products (actual or planned), know how, 
                 inventions, research and development activities, trade secrets and industrial secrets, and also 
                 confidential commercial information such as investments, investors, employees, customers, suppliers, 
                 marketing plans, etc., all the above - whether documentary, written, oral or computer generated. 
                 Proprietary Information shall also include information of the same nature which the Company may 
                 obtain or receive from third parties. Proprietary Information shall be deemed to include any 
                 and all proprietary information disclosed by or on behalf of the Company and irrespective of 
                 form but excluding information that (i) was known to Service Provider prior to Service Provider's 
                 association with the Company and can be so proven; (ii) is or shall become part of the public 
                 knowledge except as a result of the breach of the Agreement or this Exhibit by Service Provider; 
                 (iii) reflects general skills and experience gained during Service Provider's engagement by the Company
                 ; or (iv) reflects information and data generally known in the industries or trades in which the Company
                  operates.</p><p className="indented">2.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Service Provider recognizes 
                  that the Company received and will receive confidential or proprietary information from third parties, 
                  subject to a duty on the Company's part to maintain the confidentiality of such information and to 
                  use it only for certain limited purposes. In connection with such duties, such information 
                  shall be deemed Proprietary Information hereunder,<i>mutatis mutandis</i>.
                  </p><p className="indented">2.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Service Provider agrees that all 
                  Proprietary Information, and patents, trademarks, copyrights and other intellectual property 
                  and ownership rights in connection therewith shall be the sole and exclusive property 
                  of the Company and its assigns. At all times, both during Service Provider's engagement 
                  by the Company and thereafter, Service Provider will keep in strict confidence and trust 
                  all Proprietary Information and may disclose such Proprietary Information only to 
                  employees of Service Provider who have a strict need-to-know with respect to such 
                  Proprietary Information and who have executed similar but no less stringent 
                  non-disclosure obligations as those set forth herein. The Service Provider 
                  shall not to copy, duplicate, divulge, reproduce, disclose or disseminate to any third 
                  party any Proprietary Information of the Company and upon the termination of the 
                  Agreement for any reason whatsoever, Service Provider shall return to the Company 
                  ll originals and copies of the Proprietary Information then in Service Provider's 
                  possession or under Service Provider's control.</p><p className="indented">
                  2.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Service Provider's undertakings set forth in 
                  this Section 2 shall remain in full force and effect after termination of this 
                  Exhibit or any renewal thereof.</p><p>3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong><u>
                    Disclosure and Assignment of Inventions</u></strong></p><p className="indented">
                    3.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "<strong>Inventions</strong>" means any and all 
                    works of authorship, Deliverables, inventions, improvements, designs, concepts, techniques, methods, systems, 
                    processes, know how, computer software programs, databases, mask works and trade secrets, 
                    whether or not patentable, copyrightable or protectable as trade secrets; "
                    <strong>Company Inventions</strong>" means any Inventions that are made or conceived or 
                    first reduced to practice or created by Service Provider, whether alone or jointly with others, 
                    during the period of Service Provider's engagement with the Company, and which: (i) are 
                    developed using equipment, supplies, facilities or Proprietary Information of the Company, 
                    or (ii) result from or are related to work performed by Service Provider for the Company 
                    or its customers or under this Agreement.
                    </p><p className="indented">3.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Service Provider undertakes 
                    and covenants that Service Provider will promptly disclose in confidence to the Company all 
                    Inventions deemed as Company Inventions.</p><p className="indented">
                    3.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Service Provider hereby irrevocably transfers and 
                    assigns to the Company all worldwide patents, patent applications, copyrights, mask works, 
                    trade secrets and other intellectual property rights in any and all Company Invention, and any and all 
                    moral rights that Service Provider may have in or with respect to any Company Invention. 
                    Service Provider agrees to assist the Company, at the Company's expense, in every proper 
                    way to obtain for the Company and enforce patents, copyrights, mask work rights, and other 
                    legal protections for the Company Inventions in any and all countries. Service Provider 
                    will execute any documents that the Company may reasonably request for use in obtaining 
                    or enforcing such patents, copyrights, mask work rights, trade secrets and other legal 
                    protections. Such obligation shall continue beyond the termination of Service Provider's 
                    engagement with the Company. To the extent Service Provider is unable under applicable law to assign any rights as set forth
                    herein, Service Provider hereby grants Company a perpetual, exclusive, royalty-
                    free, worldwide and sublicenseable license to exploit Company Inventions in any
                    manner and through any media. The Service Provider shall indemnify and hold harmless the Company, 
                    the customer and the users of the end products against all claims, expense, demands, suits, 
                    losses, and liabilities, including costs and attorney's fees, resulting from actual or 
                    alleged infringement of any patent, trademark, copyright, or trade secret, arising from or 
                    related to the use, sale, manufacture or disposal of the Deliverables provided hereunder. 
                    Service Provider hereby irrevocably designates and appoints the Company and its 
                    authorized officers and agents as Service Provider's agent and attorney in fact, 
                    coupled with an interest to act for and on Service Provider's behalf and in Service Provider's 
                    stead to execute and file any document needed to apply for or prosecute any patent, 
                    copyright, trademark, trade secret, any applications regarding same or any other right or 
                    protection relating to any Proprietary Information (including Company Inventions), 
                    and to do all other lawfully permitted acts to further the prosecution and issuance of 
                    patents, copyrights, trademarks, trade secrets or any other right or protection relating 
                    to any Proprietary Information (including Company Inventions), with the same legal force 
                    and effect as if executed by Service Provider himself/herself/itself. The foregoing
                    assignments do not apply to Deliverables that Company has rejected in writing 
                    pursuant to Exhibit A.</p><p>
                      4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Service Provider agrees and undertakes that 
                      during the Term and for a period of twelve (12) months following termination of 
                      this engagement for whatever reason, the Service Provider will not, directly or 
                      indirectly, including personally or in any business in which the Service Provider 
                      may be an officer, director, shareholder or employee, (i) solicit for employment 
                      any person who is employed by the Company, or any person retained by the Company as a 
                      consultant, advisor or the like who is subject to an undertaking towards the Company 
                      to refrain from engagement in activities competing with the activities of the Company 
                      (for purposes hereof, a "<strong>Consultant</strong>"), or was retained as an employee 
                      or a Consultant during the six months preceding termination of the Service Provider's 
                      engagement with the Company; or (ii) solicit, canvass or approach or endeavor to solicit, 
                      canvass or approach any person who was provided with services by the Company during the 
                      Term or for a period of 12 months thereafter for the purpose of offering services or 
                      products which compete with the services or products supplied by the Company.</p>
    </div>
  </AgreementBox>
);
