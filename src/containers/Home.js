import React from 'react'
//
import Banner from '../components/Home/Banner'
import Hero from '../components/Home/Hero'
import ValueProps from '../components/Home/ValueProps'
import FAQ from '../components/Home/FAQ'
import Testimonials from '../components/Home/Testimonials'

export default () => (
  <div>
    <Banner />
    <Hero />
    <ValueProps />
    <FAQ />
    <Testimonials />
  </div>
);
