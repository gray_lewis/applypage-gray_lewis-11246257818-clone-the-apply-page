import styled from "styled-components";
import { Colors } from "../consts/styles";

const AppStyles = styled.div`
  .container {
    position: relative;
    width: 1000px; 
    z-index: 1;
    
    @media (max-width: 1025px){
      padding: 0 3rem;
      width: calc(100% - 6rem);
    }
    
    @media (max-width: 768px){
      padding: 0 3rem;
      width: calc(100% - 6rem);
    }
    
    @media (max-width: 415px){  
      padding: 0 1.5rem;
      width: calc(100% - 3rem);
    }
    
    @media (max-width: 321px){
      padding: 0 1rem;
      width: calc(100% - 2rem);
    }
  }
  
  h1 {
    font-size: 3.15rem;
    font-weight: 500;
    color: ${Colors.WHITE};
    line-height: 4rem;
  }
  
  h2{
    font-size: 1.75rem;
    font-weight: 300;
    line-height: 2.25rem;
    color: ${Colors.WHITE}
  }
  
  h3{
    color: ${Colors.LIGHT_BLACK};
    font-size: 1.875rem;
    font-weight: 500;
    line-height: 2.5rem;
  }
  
  h4{
    color: ${Colors.LIGHT_BLACK};
    font-size: 1.55rem;
    font-weight: 500;
  }
  
  h5{
    color: ${Colors.LIGHT_BLACK};
    font-size: 1.375rem;
    font-weight: 500;
  }
  
  p{
    color: ${Colors.LIGHT_BLACK};
    font-size: 1rem;
    line-height: 1.5rem;
  }
  
  .btn-cta{
    background-color: ${Colors.ORANGE};
		border-radius: 6px;
		width: 160px;
		height: 50px;
		font-size: 1rem;
		color: #fff;
		font-weight: 500;
		text-transform: uppercase;
		outline: none;
		border: 0;
		box-shadow: 2px 2px 5px rgba(51, 51, 51, .5);
		text-decoration: none;
		display: flex;
		align-items: center;
		justify-content: center;
		transition: .2s ease-in-out;
		
		&:hover{
		  filter: brightness(1.1);
		}
		
		&:active{
		  filter: brightness(.9);
		}
  }
`;

export default AppStyles;