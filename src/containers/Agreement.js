import React from 'react'
import styled from 'styled-components'
import {Colors} from "../consts/styles";
//
import AgreementBox from '../components/Agreement/AgreementBox'
import AgreementForm from '../components/Agreement/AgreementForm'

const Agreement = styled.section`
  background-color: ${ Colors.LIGHT_GRAY };
  display: flex;
  justify-content: center;
  padding: 5rem 0;
  
  h1{
    font-size: 1.5rem !important;
    color: ${ Colors.LIGHT_BLACK } !important;
    line-height: 2.25rem;
  }
`;

export default () => (
  <Agreement>
    <div className="container">
      <h1>CodersClan Engagement Agreement</h1>
      <p>Please read below agreement and fill in your details</p>
      <AgreementBox />
      <AgreementForm />
    </div>
  </Agreement>
);
