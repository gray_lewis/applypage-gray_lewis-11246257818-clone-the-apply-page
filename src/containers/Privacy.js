import React from 'react'
import styled from "styled-components";
import { Link } from 'react-static';
import { Colors } from "../consts/styles";
//

const Privacy = styled.section`
  background-color: ${ Colors.LIGHT_GRAY };
  display: flex;
  justify-content: center;
  padding: 6rem 0;
  
  h1{
    color: ${ Colors.LIGHT_BLACK } !important;
    font-weight: 400 !important;
    font-size: 2rem !important;
  }
  
  strong{
    font-weight: bold;
  }
  
  .uppercase{
    text-transform: uppercase;
  }
`;

export default () => (
  <Privacy>
    <div className="container">
      <h1>CodersClan Privacy Policy</h1>
      <p><span>CodersClan, Ltd. (“<strong>CodersClan</strong>”,
            or , “<strong>us</strong>”, “<strong>our</strong>”, “<strong>we</strong>”) provides</span><span>a crowd-sourced, expert-level
technical support forum, where developers can post coding problems for a bounty
(the “<strong>Site</strong>”</span><span>).</span><span>CodersClan respects your right to privacy. Your
ability to make informed choices about the uses of your information is
important to us. This privacy policy details how we collect, use and store
information and data that we receive.</span><span>If you have any
comments or questions about our privacy policies, please contact us at <a href="mailto:support@codersclan.net">support@codersclan.net</a>.</span></p><br/><p><span>As used in this privacy policy, “<strong>Personal
Information</strong>” means information that can be directly associated with a
            specific person or entity such as a name, address, telephone number, or e-mail
            address. Other Terms capitalized but not defined in this Privacy Policy shall
            have the meaning set forth in our Terms of Service, a current version of which
            is available <Link to="/terms" target="_blank"> here</Link> (the “<strong>Terms</strong>”).</span></p><br/><h4 id="nav-appear">Information We Collect</h4><br/><p><span>In
order to use the Site as an Expert or a Poster, you will be required to log in
through a specified social network or provide specified information. If you log
in through a social network, we will receive such Personal Information about
you as is provided by such social network. Please check the policies of such
social network in order to understand what information we receive. If you
create an account with us, we receive and store all information that you
provide in the creation of the account. We also collect Personal Information when
you make use of the Site, including when you propose a problem or a Solution,
and includes comments or ratings that you make post on the Site. You are not
required by law to provide us with any such information, but by using the
Application and agreeing to the Terms you agree to provide us with such
information.</span></p><br/><p><span>The
Site will also record certain information automatically, such as your IP
address, mobile device or computer, operating system and browser. In addition, we
collect any information you provide when you contact us for any reason. You are
not required by law to provide us with any of the information described herein,
but by using the Site you agree to provide us with such information.</span></p><p><span>We may use
services such as Google Analytics that provides us with analysis of the use of
our Site.</span></p><br/><h4>How We Use Information</h4><br/><p><span>We
will not share your Personal Information with third parties without your
explicit permission, except when required by law, regulation, subpoena or court
order or as otherwise expressly set forth herein. We may use Personal
Information and other information internally – for example, to help diagnose
problems with our servers, and to make the Services more useful for you and for
our other customers.</span></p><br/><p><span>By analyzing all information we receive, we may
compile statistical information across a variety of users (“<strong>Statistical
Information</strong>”).<strong></strong>Statistical Information helps understand trends and
            customer needs so that new services can be considered and so the Servicess and associated
            services can be tailored to customer desires. We may share Statistical
            Information with our partners, pursuant to commercial terms that we determine
            in our sole discretion. In addition, we may provide any information to third
            parties, so long as we have removed any information that identifies any specific
            individual or user, such as a name, address or contact information</span><span>.</span></p><br/><h4>How we Protect Information</h4><br/><p><span>We follow generally accepted industry standards to
protect the Personal Information submitted to us, both during transmission and
once we receive it. However, no method of transmission over the Internet, or
method of electronic storage is 100% secure. Therefore, while we strive to use
commercially acceptable means to protect your Personal Information, we cannot
guarantee its absolute security.</span></p><br/><p><span>We will delete any Personal Information provided to
us by a user upon the receipt of a written request by such user. We cannot
restore information once it has been deleted.</span></p><br/><h4>Cookies</h4><br/><p><span>A
cookie is a small piece of text that is sent to a visitor's browser. The
browser provides this piece of text to the device of the originating visitor
when this visitor returns. A “persistent” cookie may be used to help save your
settings and customizations across visits. Most Web browsers are initially
configured to accept cookies, but you can change this setting so your browser
either refuses all cookies or informs you when a cookie is being sent. Also,
you are free to delete any existing cookies at any time. Please note that some
features of the Site may not function properly when cookies are disabled or
removed.</span></p><br/><h4>Third Party Sites and Services</h4><br/><p><span>We
are not responsible for the use of any data by third parties, and we cannot
vouch for the privacy policies of any third party. The Services may link
to or use web sites or services belonging to third parties. We have no control
over third-party sites or services, and all use of third-party sites or services
is at your own risk. We cannot accept responsibility for the privacy policies
of any such sites. We are not responsible for content available by means of
such sites or services. We do not endorse any services or offered by third
parties and we urge our users to exercise caution in using third-party sites or
services. When we provide information to
            third parties, we remove any information that identifies any particular
            individual or user, such as a name, address or contact information, but we
            cannot provide complete assurance that the recipient will not be able to
            associate such de-identified information with a particular individual.</span><span></span></p><br/><h4>Children</h4><br/><p><span>We
do not knowingly collect Personal Information from children under the age of
14. In the event you become aware that an individual under the age of 14 has
enrolled without parental permission, please advise us immediately.</span></p><br/><h4>Other Uses or Transfer of Your Information</h4><br/><p>We will fully cooperate with any law enforcement authorities or court
        order requesting or directing us to disclose the identity or behavior of any
        user suspected to have engaged in illegal behavior.</p><p><span>We may use subcontractors that are located in
countries other than your own, and send them information we receive. For
example, we may send your information to any countries outside the United
States, Canada or the European Union. All our subcontractors will be subject to
non-disclosure and non-use obligations. Nevertheless, some countries may have
levels of protection of personal and other information which are not as high as
in your country of residence or business.</span></p><p>We may transfer our databases containing information if we sell our
        business or part of it.</p><br/><h4>Changes</h4><br/><p><span>We
may update this privacy policy from time to time, and we encourage you to
review it periodically.</span></p><br/><h4>Comments and Questions</h4><br/><p><span>If
you have any comments or questions about our privacy policy, please contact us
at </span><span><a href="mailto:support@codersclan.net">support@codersclan.net</a></span></p><p><em><span>Last
updated: January 2014</span></em></p></div>
  </Privacy>
);
