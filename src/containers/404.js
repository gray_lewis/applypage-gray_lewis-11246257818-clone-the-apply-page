import React from 'react'
import styled from 'styled-components'
import {Colors} from "../consts/styles";
import { Link } from 'react-static';
//

const Container = styled.section`
  background-color: ${ Colors.LIGHT_GRAY };
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  min-height: 100vh;
  text-align: center;
  box-sizing: border-box;
  padding: 3rem 0;
  
  h1{
    color: ${ Colors.LIGHT_BLACK } !important;
    //margin: 0 auto;
    font-size: 2rem !important;
  }
  
  h2{
    color: ${ Colors.LIGHT_BLACK } !important;
    margin-bottom: 2rem;
  }
  
  @media (max-width: 415px){
    
    box-sizing: content-box;
    
    img{
      width: 100%;
    }
  }
  
`;

export default () => (
  <Container>
    <div className="container">
      <h1>Oh this is weird, we couldn't find what you were looking for...</h1>
      <h2>Maybe we should start over on the <Link to={"/"}>home page</Link>.</h2>
      <img src="https://media.giphy.com/media/3o6Zt4MUD2q0R3CaPu/giphy.gif" alt=""/>
    </div>
  </Container>
)
