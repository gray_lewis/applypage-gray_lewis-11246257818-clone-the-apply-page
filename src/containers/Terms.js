import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-static';
import {Colors} from "../consts/styles";
//

const Terms = styled.section`
  background-color: ${ Colors.LIGHT_GRAY };
  display: flex;
  justify-content: center;
  padding: 6rem 0;
  
  h1{
    color: ${ Colors.LIGHT_BLACK } !important;
    font-weight: 400 !important;
    font-size: 2rem !important;
  }
  
  strong{
    font-weight: bold;
  }
  
  .uppercase{
    text-transform: uppercase;
  }
`;

export default () => (
  <div>
    <Terms>
      <div className="container">
        <h1>CodersClan Terms of Service</h1>
        <p><span>[CodersClan Inc.] (“<strong>CodersClan</strong>”,
            or “<strong>us</strong>”, “<strong>our</strong>”, “<strong>we</strong>”) provides a crowd-sourced,
            expert-level technical support forum, where developers can post coding problems
            for a bounty (the “<strong>Site</strong>”). These Terms of Service (“<strong>Terms</strong>”)
            govern your access and use of the Site. “<strong>You</strong>” means a user of the Site,
            including without limitation any entity or individual posting a problem or any
            entity or individual offering a solution.</span></p><br/><p><span>Please read these Terms carefully.
You must accept these Terms prior to using the Site. By using the Site, you
signify your consent to these Terms, including without limitation all terms
herein regarding our collection, use and storage of data, which we may modify
from time to time, a current version of which is available <Link to="/privacy-policy" target="_blank">here</Link> (our “<strong>Privacy
Policy</strong>”).</span><span>Changes
may be made to these Terms and the Privacy Policy from time to time. Your
continued use of the Site after any such modification will be deemed acceptance
to any such amended or updated terms.</span><span>If
you do not agree to any of these Terms, please do not make any use of the Site.</span></p><br/><h4>Use of Site</h4><br/><p><span>CodersClan allows you to access and
use the Site subject to these Terms. CodersClan may decide to change or modify
the Site, or the services provided through the Site, at any time and without
notice. CodersClan may discontinue providing the Site or any part thereof without
notice.</span></p><br/><p><span>Use of and access to the Site is
void where prohibited by law. By using the Site, you represent and warrant that
(a) any and all registration information you submit is truthful and accurate;
(b) you will maintain the accuracy of such information; (c) you either are 18
years of age or older, or else have the permission of your parents or guardians
to use the Site and (d) your use of the Site does not violate any applicable
law or regulation or any obligation.</span></p><br/><p><span>CodersClan may at its discretion
monitor and record activities in any forum of the Site, including Private
Forums. CodersClan is not obligated to monitor any forum and is not obligated
to ensure that the content in any forum complies with applicable law or these
Terms.</span></p><br/><h4>Registration</h4><br/><br/><p><span>You may register to use CodersClan
by connecting through certain third party accounts, such as Google+ or GitHub,
or by opening an account with us.</span><span>By
registering through a third party account you represent and warrant that such
account is yours and you have all right to provide us with the information in
such account according to applicable terms and conditions</span><span>. If you register directly with us,
you represent and warrant that all information that you provide is up-to-date
and accurate, and you will ensure that such information remains up-to-date and
accurate.</span></p><br/><p><span>We may terminate your use of the Site
if you let someone use your account inappropriately, or if you or anyone using
your account violates these Terms.</span><span>If
your account is terminated, you may not rejoin CodersClan again without our express
permission.</span><span>You agree
to immediately notify us of any unauthorized use of your account. You are fully
and solely responsible for the security of your computer system, mobile device
and all activity on your account, even if such activities were not committed by
you. CodersClan will not be liable for any losses or damage arising from
unauthorized use of the Site, and you agree to indemnify and hold CodersClan
harmless for any improper or illegal use of the Site, and any charges and taxes
incurred, unless you have notified us via e-mail at </span><a href="mailto:support@codersclan.net"><span>support@codersclan.net</span></a><span>that your account have been
compromised</span><span>.</span><span>We do not police for, and cannot
guarantee that we will learn of or prevent, any inappropriate use of the Site.</span></p><br/><h4>Termination of Account</h4><br/><p><span>You agree that CodersClan may for
any reason, in its sole discretion and without notice, terminate your account,
and remove from the Site any User Content (as defined below) associated with
your account. Grounds for such termination may include (i) extended periods of
inactivity, (ii) violation of the letter or spirit of these Terms, (iii)
fraudulent, harassing or abusive behavior, or (iv) behavior that is harmful to
other users or the business interests of CodersClan. If you account is
terminated, CodersClan may remove and delete all content, including any
problems and Solutions, associated with your account.</span></p><br/><p><span>If CodersClan believes, in its sole
discretion, that a violation of these Terms or any illegal or inappropriate
behavior has occurred, we may take any other corrective action we deem
appropriate. We reserve the right to investigate suspected violations of these
Terms or illegal and inappropriate behavior on the Site. We will fully cooperate
with any law enforcement authorities or court order requesting or directing us
to disclose the identity or behavior of anyone believed to have violated these
Terms or to have engaged in illegal behavior on the Site. You may request
termination of your CodersClan account at any time and for any reason by
sending an email to </span><a href="mailto:support@codersclan.net"><span>support@codersclan.net</span></a><span>. The provisions hereof regarding
intellectual property, confidentiality, indemnification, disclaimer of warranties
and limitation of liabilities shall survive the termination or expiration of
these Terms for any reason. In addition, for the avoidance of doubt, all
assignments effected by you and licenses granted by you during the term that
these Terms were in effect shall survive the termination of these Terms for any
reason.</span></p><br/><h4>Your Obligations and Responsibilities</h4><br/><p><strong><span>Definitions.</span></strong><span>In these Terms, (a) an “<strong>Expert</strong>”
            means an individual or entity posting a Solution on the Site; (b) a “<strong>Poster</strong>”
            means an individual or entity posting a problem on the Site; (c) a “<strong>Solution</strong>”
            means any solution posted on the Site, including all code, documentation and
            other materials posted on the Site as part of or in addition to such solution; (d)
            a “<strong>Private Forum</strong>” means a forum for posting problems, where the
            functionality of the Site only allows the Poster (or a third party authorized
            by the Poster) to view Solutions and (e) a “<strong>Public Forum</strong>” means all
            forums other than Private Forums.</span></p><br/><p><strong><span>Attribution:</span></strong><span>If you display or distribute any
code you receive from the Site, you must include the following attribution
statement: “This software includes software code obtained from codersclan.com”.
This attribution statement is in addition to any other notices (such as
copyright notices) that must be retained under any applicable law or license.
All users of the Site acknowledge the responsibility to include this
attribution statement, and no user shall have any claims against CodersClan or
any other third party in respect of the inclusion of such attribution
statement. The foregoing attribution statement shall not apply to code used as
part of a commercial product.</span></p><br/><p><strong><span>Postings:</span></strong><span>If you post any problem on the Site,
you agree to provide a reasonably fair and accurate description of the problem,
with sufficient detail to allow a developer experienced in the field to
understand the problem and understand the amount of resources that need to be expended
in solving the problem. In addition, you agree to make prompt payment of any
bounty that you offered through the Site, in full compliance with these Terms,
in consideration for the Solution you accept. You may not use, distribute,
display, publish or make available any posted Solution unless you have rights
to use that Solution pursuant to these Terms, except that you may use any
Solution solely to the extent necessary to test such Solution for compliance
with your provided specifications and only pursuant to written confidentiality
agreements that ensure that any third party examining such Solution shall
comply with all of your obligations under these Terms concerning
confidentiality and intellectual property. You represent and warrant that,
prior to making payment of any bounty, that (a) you have examined the proposed
Solution and found it suitable for your needs, (b) you expressly waive all
claims against the Expert or CodersClan that the accepted Solution does not
comply with your specifications or satisfy your needs and (c) that you have
reviewed the warranties and representations made by the applicable Expert
according to these Terms and found them sufficient and satisfactory.</span></p><br/><p><strong><span>Experts</span></strong><span>: If you post any Solution on the
Site, you represent and warrant that any such Solution is either solely of your
original authorship or, to the best of your knowledge after reasonable
investigation, that you have sufficient rights to post such Solution and provide
Posters with all rights in such Solution as required by these Terms. For the
avoidance of doubt, you are not required to perform a patent search to
determine whether any proposed Solution infringes any patents or patent rights.
In addition, you represent and warrant that no Solution that you provide
through the Site will contain any (a) worms, viruses, Trojan horses, time bombs
or any other malicious or dangerous functionality, or (b) third party or open
source software not expressly disclosed by you as part of the Solution in the
same forum in which you posted the Solution. Any violation by you of these
requirements may result in the immediate termination of your account. CodersClan
will not hesitate to take additional legal action where appropriate. During the
time that any Solution is available for viewing on the Site, you shall not
transfer any rights in such Solution except pursuant to these Terms. An Expert
may request the deletion of any posted Solution by contacting CodersClan, which
may delete such Solution in its sole discretion. CodersClan’s policy is not to
delete any Solution that has been posted for less than one week on the Site.<span className="uppercase">Except as expressly set forth herein, an
Expert makes no representations and warranties, and expressly disclaims any
statutory or implied warranties of merchantability, fitness for a particular
purpose or non-infringement in respect of any Solution.</span>Experts must
            have Payoneer accounts in order to receive payments. CodersClan shall have no
            obligation to make payment in any manner except to a Payoneer account. If an
            Expert does not have a Payoneer account, CodersClan will hold payment amounts for
            no longer than one year until an Expert obtains a Payoneer account. Unpaid
            amounts shall be forefeited&nbsp; if the Expert has not provided CodersClan with
            notice of the applicable Payoneer account by the conclusion of such one year
            period. </span></p><br/><a id="rights-in-solutions"></a><h4>Rights in Solutions</h4><br/><p><strong><span>Rights in Solutions Posted in
Private Forums:</span></strong><span>Postings
may be posted in Private Forums.&nbsp; A posting in a Private Forum may state that
it is offering a specific bounty in consideration for rights to an accepted Solution
pursuant to the Apache 2.0 license. Alternatively, a posting in a Private Forum
may state that it is offering a specific bounty in consideration for full
ownership of the accepted Solution. Experts will retain all right, and title in
any Solutions posted in Private Forums, until and unless that Solution is
accepted by the applicable Poster and the applicable Poster makes payment in
respect thereof in accordance with these Terms.</span></p><br/><p><span>If an Expert has been informed by
the Poster through the Site that its Solution posted in a Private Forum has
been accepted, and the posting stated that the specific bounty is being offered
in consideration for rights to such Solution pursuant to the Apache 2.0
license, then such Expert hereby grants the applicable Poster all rights in the
Solution pursuant to the Apache 2.0 license. Such license is subject to the
Expert being paid the specified bounty according to the terms and conditions of
these Terms. As such, if the Poster does not make the required payment within
30 days of the due date set forth in these terms, the license set forth herein
shall be retroactively terminated. &nbsp;This license, if not retroactively
terminated, survives the termination or expiration of these Terms for any
reason.</span></p><br/><p><span>If an Expert has been informed by
the Poster through the Site that its Solution posted in a Private Forum has
been accepted, and the posting stated that the specific bounty is being offered
in consideration for full ownership of the Solution, then such Expert hereby
assigns the applicable Poster all right, title and interest in the Solution
(including any moral or similar rights); to the extent any of the rights, title
and interest in and to Solution (including any moral or similar rights) cannot
be assigned under applicable law by the Expert to the Poster, the Expert hereby
grants to the Poster an exclusive, royalty-free, transferable, irrevocable,
worldwide, fully paid-up license (with rights to sublicense through multiple
tiers of sublicensees) to fully use, practice and exploit those non-assignable
rights, titles and interests, including, but not limited to, the right to make,
use, sell, offer for sale, import, have made, and have sold, the Solution;
to the extent any of the rights, title and interest in and to the Solution (including
any moral or similar rights) can neither be assigned nor licensed by the Expert
to the Poster, the Expert hereby irrevocably waives and agrees never to assert
the non-assignable and non-licensable rights, title and interest against the Poster,
any of its successors in interest, or any of its customers; in no event shall the
Expert make any use of the Solution save as for the benefit of the Poster as
expressly set forth herein; the Expert agrees to perform, at the Poster’s
reasonable expense, during and after the term of this Terms, all acts that the Poster
deems necessary or desirable to permit and assist the Poster in obtaining,
perfecting and enforcing the full benefits, enjoyment, rights and title
throughout the world in the Solution pursuant to these terms;&nbsp;if the Poster
is unable for any reason to secure the Expert’s signature to any document
required to file, prosecute, register or memorialize the assignment of any
rights as provided under these Terms, the Expert hereby irrevocably designates
and appoints the Poster (or its duly authorized officers and agents) as its agents
and attorneys-in-fact to act for and on its behalf and instead of it to take
all lawfully permitted acts to further the filing, prosecution, registration,
memorialization of assignment, issuance and enforcement of rights in, to and
under the Solution, all with the same legal force and effect as if executed by the
Expert.&nbsp; The foregoing is deemed a power coupled with an interest and is
irrevocable. The foregoing assignment and licenses are subject to the Expert
being paid the specified bounty according to the terms and conditions of these
Terms. As such, if the Poster does not make the required payment within 30 days
of the due date set forth in these Terms, the assignment and licenses set forth
herein shall be retroactively terminated and of no effect. All assignments and
licenses set forth herein, if not retroactively terminated, survive the
termination or expiration of these Terms for any reason. CodersClan does not
guarantee that an Expert will actually perform any of the actions set forth in
this provision, and does not accept any liability if an Expert does not perform
any of such actions.</span></p><br/><p><span>In a Private Forum, if a Poster
chooses to use several Solutions, the Poster shall be obligated to make payment
of the full bounty in respect of each Solution unless the Experts that have
posted the applicable Solutions have expressly agreed otherwise.</span></p><br/><p><strong><span>Rights in Solutions Posted in Public
Forums:</span></strong><span>Postings
may be posted in Public Forums.&nbsp; A posting in a Public Forum may state that it
is offering a specific bounty in consideration for rights to an accepted
Solution pursuant to the Apache 2.0 license. Alternatively, a posting in a
Public Forum may state that it is offering a specific bounty in consideration
for full ownership of the accepted Solution. Experts retain all rights to
Solutions posted in Public Forums, subject to the terms hereof.</span></p><br/><p><span>If an Expert has been informed by
the Poster through the Site that its Solution posted in a Public Forum has been
accepted, and the posting stated that the specific bounty is being offered in
consideration for rights to such Solution pursuant to the Apache 2.0 license,
then upon payment of the specified bounty according to the terms and conditions
of these Terms, all Experts that have posted Solutions in such Public Forum
grant any and all third parties the right to use each and every Solution posted
in such Public Forum pursuant to the Apache 2.0 license. Alternatively, if more
than 7 days have passed since the last Solution (or modification thereof) was
posted in the Public Forum, then any user of the Site may accept a Solution by
making payment of the applicable bounty to the applicable Expert. Upon payment
of the specified bounty according to the terms and conditions of these Terms by
such third party, all Experts that have posted Solutions in such Public Forum
grant any and all third parties the right to use each and every Solution posted
in such Public Forum pursuant to the Apache 2.0 license. Any license granted
hereunder shall survive the termination or expiration of these Terms for any
reason. Payment of the bounty in this paragraph may be satisfied by making
payment of portions or percentages of the bounty to any number of third party
Experts that have posted Solutions in the Public Forum, so long as the entire
amount of the bounty is paid to third party Experts that have posted Solutions
in the Public Forum.</span></p><br/><p><span>If an Expert has been informed by
the Poster through the Site that its Solution posted in a Public Forum has been
accepted, and the posting stated that the specific bounty is being offered in
consideration for full ownership of the Solution, then upon payment of the
specified bounty such Expert hereby assigns the applicable Poster all right,
title and interest in the Solution (including any moral or similar rights); to
the extent any of the rights, title and interest in and to Solution (including
any moral or similar rights) cannot be assigned under applicable law by the
Expert to the Poster, the Expert hereby grants to the Poster an exclusive,
royalty-free, transferable, irrevocable, worldwide, fully paid-up license (with
rights to sublicense through multiple tiers of sublicensees) to fully use,
practice and exploit those non-assignable rights, titles and interests,
including, but not limited to, the right to make, use, sell, offer for sale,
import, have made, and have sold, the Solution;&nbsp; to the extent any of the
rights, title and interest in and to the Solution (including any moral or
similar rights) can neither be assigned nor licensed by the Expert to the
Poster, the Expert hereby irrevocably waives and agrees never to assert the
non-assignable and non-licensable rights, title and interest against the Poster,
any of its successors in interest, or any of its customers; in no event shall the
Expert make any use of the Solution save as for the benefit of the Poster as
expressly set forth herein; the Expert agrees to perform, at the Poster’s
reasonable expense, during and after the term of this Terms, all acts that the
Poster deems necessary or desirable to permit and assist the Poster in
obtaining, perfecting and enforcing the full benefits, enjoyment, rights and
title throughout the world in the Solution pursuant to these terms;&nbsp;if the
Poster is unable for any reason to secure the Expert’s signature to any
document required to file, prosecute, register or memorialize the assignment of
any rights as provided under these Terms, the Expert hereby irrevocably
designates and appoints the Poster (or its duly authorized officers and agents)
as its agents and attorneys-in-fact to act for and on its behalf and instead of
it to take all lawfully permitted acts to further the filing, prosecution,
registration, memorialization of assignment, issuance and enforcement of rights
in, to and under the Solution, all with the same legal force and effect as if
executed by the Expert.&nbsp; The foregoing is deemed a power coupled with an
interest and is irrevocable. The foregoing assignment and licenses are subject
to the Expert being paid the specified bounty according to the terms and
conditions of these Terms. As such, if the Poster does not make the required
payment within 30 days of the due date set forth in these Terms, the assignment
and licenses set forth herein shall be retroactively terminated and of no
effect. All assignments and licenses set forth herein, if not retroactively
terminated, survive the termination or expiration of these Terms for any
reason. CodersClan does not guarantee that an Expert will actually perform any
of the actions set forth in this provision, and does not accept any liability
if an Expert does not perform any of such actions. Upon payment of the
specified bounty according to the terms and conditions of these Terms, all
Experts that have posted Solutions in such Public Forum (except the Expert
whose Solution was accepted) grant any and all third parties the right to use each
and every Solution posted in such Public Forum pursuant to the Apache 2.0
license. Alternatively, if more than 7 days have passed since the last Solution
(or modification thereof) was posted in the Public Forum, then any user of the
Site may accept a Solution by making payment of the applicable bounty to the
applicable Expert. Upon payment of the specified bounty according to the terms
and conditions of these Terms by such third party, all Experts that have posted
Solutions in such Public Forum grant any and all third parties the right to use
each and every Solution posted in such Public Forum pursuant to the Apache 2.0
license. Any license granted hereunder shall survive the termination or
expiration of these Terms for any reason.</span></p><br/><p><strong><span>Payment:</span></strong><span>If you accept any Solution, you
shall make full payment of any bounty offered in respect of such Solution.
Payment shall be made to CodersClan according to instructions and payment
mechanisms available on the Site, which CodersClan may modify from time to
time. CodersClan shall distribute the funds as per your instructions. All
payment distribution instructions must be clear and unambiguous, and CodersClan
shall have no liability for distributing funds based on unclear or ambiguous
instructions. CodersClan shall receive a percentage of all payments as set
forth in more detail on the Site. CodersClan may waive such percentage at any
time in its sole discretion, and any such waiver shall be set forth in more
detail on the Site and shall be interpreted narrowly to only apply as expressly
set forth therein. Payment shall be made within 10 days of your acceptance of
any Solution through the Site. You shall add VAT, sales tax, use or excise
taxes to any payments made hereunder as required by law. You shall not withhold
or deduct any amounts from payments made hereunder, except as required by
applicable law or regulation. If you are required by applicable law or
regulation to withhold or deduct any amounts from payments made hereunder, you
shall promptly inform both CodersClan and the applicable Expert of such
requirement, and you shall make payment of additional amounts such that
CodersClan or the applicable expert shall receive the full amount of the
applicable payment as if no such withholding or deduction was required. You
shall transfer all deducted or withheld amounts to the applicable government or
tax authority. You shall provide the applicable Expert, upon request, with
proof of such payment to the applicable government or tax authority. All
Posters and Experts shall make commercially reasonable efforts to cooperate
with CodersClan or each other, including completing and submitting required
documentation, and including claiming benefits under an applicable tax treaty,
to minimize or eliminate any requirement to withhold or deduct from amounts
payable hereunder. CodersClan may withhold or deduct taxes and other government
charges from payments, including as required by law.</span></p><br/><p><span>If Poster does not accept a Solution or otherwise provide substantive
feedback in respect of a Solution within 21 days of the last substantive addition
or change to the Solution then the Solution shall be automatically deemed accepted by Poster.
In such event, (a) CodersClan reserves the right to transfer any amounts prepaid
or deposited by the applicable Poster to the applicable Experts and (b) in the event
a number of Experts participated in the creation of the Solution, CodersClan may allocate
such amounts among such Experts as determined by CodersClan in its sole discretion.
If no Expert makes any substantive change to a Solution within 21 days of a Poster
providing substantive feedback in respect of the Solution then CodersClan may in its
sole discretion refund to Poster any applicable amounts not yet transferred to an Expert.
For the avoidance of doubt, if CodersClan refunds any such amounts in respect of a Solution
then unless expressly stated otherwise herein Poster shall have no rights in respect of
the applicable Solution.</span></p><br/><h4>Intellectual Property</h4><br/><p><span>Aside from any Solutions available
on the Site, CodersClan and its licensors own the Site, including all worldwide
intellectual property rights in the Site, and the trademarks, service marks,
and logos contained therein.&nbsp; Except as expressly permitted herein, you
may not copy, further modify, duplicate, distribute, display, perform,
sublicense, republish, retransmit, reproduce, create derivative works of,
transfer, sell or otherwise use the Site or any User Content (as defined below)
appearing in the Site.&nbsp; You will not remove, alter or conceal any
copyright, trademark, service mark or other proprietary rights notices
incorporated in the Site. All trademarks are trademarks or registered
trademarks of their respective owners. Nothing in these Terms grants you any
right to use any trademark, service mark, logo, or trade name of CodersClan or
any third party.</span></p><br/><p><span>You may not or attempt to (a)
decipher, decompile, disassemble, or reverse-engineer any of the software used
to provide the Site; (b) circumvent, disable, or otherwise interfere with
security-related features of the Site or features that prevent or restrict use
or copying of any User Content; (c) use any robot, spider, site search or
retrieval service, or any other manual or automatic device or process to
retrieve, index, data-mine, or in any way reproduce or circumvent the
navigational structure or presentation of the Site or any content available on
the Site, except pursuant to and in compliance with the applicable robot.txt
file on the Site; (d) harvest, collect or mine information about users of the Site;
or (e) use or access another user’s account or password.<strong></strong></span></p><br/><h4>User Communications, Reviews and
          Ratings</h4><br/><p><span>CodersClan allows you to post descriptions
of problems on the Site, as well as proposed Solutions. In addition, CodersClan
may allow you to post reviews and ratings of other users, or of problems and
Solutions. We refer to all of the foregoing, as well as any other content
posted or transmitted by any user of the Site, as “<strong>User Content</strong>”.</span></p><br/><p><span>CodersClan has no obligation to
accept, display, review or maintain any User Content. Moreover, CodersClan
reserves the right to edit, delete, distort or move user Content from the Site without
notice for any reason at any time. Any User Content posted or submitted in a
Public Forum shall not be considered confidential and may be disseminated by CodersClan
without any compensation to you. As such, you should have no expectation of
privacy in any content that you submit in a Public Forum. The foregoing
provision does not apply to Solutions – rights applicable to any Solution are
described above under the heading “Rights in Solutions”. CODERSCLAN DOES NOT
ENDORSE ANY CONTENT OR ANY OPINION, RECOMMENDATION, OR ADVICE EXPRESSED BY ANY
USER AND CODERSCLAN EXPRESSLY DISCLAIMS ANY AND ALL LIABILITY IN CONNECTION
WITH USER CONTENT.</span></p><br/><p><span>You are fully and solely responsible
for any User Content you submit or post. You agree that you will not act in any
way or transmit or post any User Content that: (a) restricts or inhibits use of
the Site; (b) solicits another person’s password or other personal information
under false pretenses; (c) impersonates another user or otherwise misrepresents
yourself in any manner; (d) infringes (or results in the infringement of) the intellectual
property, moral, publicity, or privacy rights of any third party; (e) is (or
you reasonably believe or should reasonably believe to be) illegal, fraudulent,
or unauthorized, or in furtherance of any illegal, counterfeiting, fraudulent,
pirating, unauthorized, or violent activity, or that involves (or you
reasonably believe or should reasonably believe to involve) any stolen,
illegal, counterfeit, fraudulent, pirated, or unauthorized material; (f) does
not comply with all applicable laws, rules, or regulations, including obtaining
all necessary permits, licenses, registrations, etc. (in the case of any
proposed or actual transaction, “applicable” refers to both your own location
and to location(s) of all other parties to the transaction); or would cause CodersClan
to be in violation of any law or regulation, or to infringe any right of any
third party; (g) contains falsehoods or misrepresentations that may damage CodersClan
or any third party; (h) imposes an unreasonably or disproportionately large
load on our infrastructure; or (i) posts, stores, transmits, offers, or
solicits anything that contains the following, or that you know contains links
to the following or to locations that in turn contain links to the following:
(i) material that we determine to be offensive (including material promoting or
glorifying hate, violence, bigotry, or any entity (past or present) principally
dedicated to such causes or items associated with such an entity), (ii)
material that is racially or ethnically insensitive, material that is
defamatory, harassing or threatening, (iii) pornography (including any obscene
material, and anything depicting children in sexually suggestive situations
whether or not depicting actual children) or may be harmful to a minor, (iv)
any virus, worm, Trojan horse, or other harmful or disruptive component or (v)
anything that encourages conduct that would be considered a criminal offense,
give rise to civil liability, violate any law or regulation or is otherwise
inappropriate.</span></p><br/><p><span>CodersClan may choose at its sole
discretion to monitor User Content for inappropriate or illegal behavior,
including through automatic means; provided, however, that CodersClan reserves
the right to treat User Content as content stored at the direction of users for
which CodersClan will not exercise editorial control except when violations are
directly br/ought to CodersClan’s attention.</span></p><br/><p><span>By submitting or posting any User Content
in a Public Forum, you grant CodersClan and its successors and assigns a
worldwide, non-exclusive, royalty-free, perpetual, sub-licensable and
transferable license under any of your intellectual property, moral or privacy
rights to use, copy, distribute, transmit, modify, prepare derivative works of,
publicly display, and publicly perform such User Content on, through or in
connection with the Site (including to post such User Content to any social
media Internet sites or services) in any media formats and through any media
channels, including without limitation, for commercially promoting the Site or
any of our services.<strong><em></em></strong>The foregoing provision does not apply to
            Solutions – rights applicable to Solutions are described under the heading
            “Rights in Solutions”.</span></p><br/><h4>Copyright Infringement</h4><br/><p><span>CodersClan respects the intellectual
property rights of others and expects its users to do the same. CodersClan will
remove all infringing User Content if properly notified that it infringes third
party intellectual property rights, and may do so at its sole discretion,
without prior notice to users at any time.</span></p><br/><p><span>Under the Digital Millennium
Copyright Act of 1998 (the “<strong>DMCA</strong>”), it is our policy to respond
            expeditiously to copyright owners who believe material appearing on the Site infringes
            their rights. &nbsp;If you believe that something appearing on the Site infringes
            your copyright, you may send us a notice requesting that it be removed, or
            access to it blocked. If you believe that such a notice has been wrongly filed
            against you, the DMCA lets you send us a counter-notice. It is CodersClan’s
            policy to terminate the account of repeat infringers in appropriate
            circumstances, taking all facts and circumstances into account. Notices and
            counter-notices must meet the DMCA’s requirements. We suggest that you consult
            your legal advisor before filing a notice or counter-notice. Be aware that
            there can be substantial penalties for false claims. Send notices and
            counter-notices to our copyright agent: copyright@codersclan.com.</span></p><br/><h4>Disclaimers &amp; Disclaimer of Warranty</h4><br/><p><span>Your use of the Site and/or User Content
is at your sole discretion and risk. The Site and/or User Content are provided
on an AS IS and AS AVAILABLE basis without warranties of any kind from CodersClan.
CODERSCLAN EXPRESSLY DISCLAIMS ALL WARRANTIES OF ANY KIND, EXPRESS, IMPLIED OR
STATUTORY, RELATING TO THE<span className="uppercase">Site</span>AND/OR
            USER CONTENT, INCLUDING WITHOUT LIMITATION THE WARRANTIES OF TITLE,
            MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. CODERSCLAN
            DISCLAIMS ANY WARRANTIES, EXPRESS OR IMPLIED, (I) REGARDING THE SECURITY,
            ACCURACY, RELIABILITY, TIMELINESS AND PERFORMANCE OF THE<span className="uppercase">Site</span>AND/OR USER CONTENT; OR (II) THAT
            THE<span className="uppercase">Site</span>WILL BE ERROR-FREE OR
            THAT ANY ERRORS WILL BE CORRECTED; OR (III) REGARDING THE PERFORMANCE OF OR
            ACCURACY, QUALITY, CURRENCY, COMPLETENESS OR USEFULNESS OF ANY INFORMATION
            PROVIDED BY THE<span className="uppercase">Site</span>. CODERSCLAN
            DOES NOT WARRANT THAT YOU WILL RECEIVE ANY USEFUL CODE OR SOLUTIONS THROUGH THE
            SITE, AND DOES NOT WARRANT THAT YOU WILL RECEIVE ANY SOLUTIONS PRIOR TO ANY
            DEADLINE SET ON THE SITE.</span></p><br/><p><span>Content on the Site originates from
a variety of sources. CODERSCLAN DOES NOT WARRANT THAT ANY OFFERED SOLUTION
SATISFIES ANY SPECIFICATIONS OR REQUIREMENTS, AND DOES NOT WARRANT THAT ANY OFFERED
SOLUTION DOES NOT INFRINGE THIRD PARTY INTELLECTUAL PROPERTY RIGHTS. CODERSCLAN
CANNOT AND DOES NOT EXAMINE EVERY SOLUTION AND DOES NOT ACCEPT RESPONSIBILITY
OR LIABILITY FOR ANY SOLUTION POSTED ON THE SITE. CODERSCLAN DOES NOT WARRANT
THAT ANY USER CONTENT IS ACCURATE, COMPLETE, RELIABLE, CURRENT, OR ERROR-FREE. CODERSCLAN
IS NOT RESPONSIBLE FOR ANY INCORRECT OR INACCURATE USER CONTENT POSTED ON THE SITE.
CODERSCLAN CANNOT AND DOES NOT ACCEPT ANY LIABILITY FOR ANY RELIANCE BY YOU ON ANY
USER CONTENT. We reserve the right to correct any errors, inaccuracies or
omissions and to change or update the User Content information at any time
without prior notice (including after you have submitted a request to borrow any
materials).</span></p><br/><p><span>YOU MUST REVIEW ALL CODE YOU HAVE
OBTAINED THROUGH THE SITE (INCLUDING ANY BINARIES RECEIVED FROM ANY USER OF THE
SITE OR ANY THIRD PARTY) TO ENSURE THAT SUCH CODE DOES NOT CONTAIN ANY VIRUS,
WORMS OR OTHER MALICIOUS CODE. CODERSCLAN DOES NOT ACCEPT ANY LIABILITY IN
RESPECT OF ANY CODE YOU MAY RECEIVE FROM ANY USER OF THE SITE OR THIRD PARTY.
ALL USE OF SUCH CODE IS AT YOUR OWN RISK.</span></p><br/><p><span>CODERSCLAN SHALL HAVE NO LIABILITY
IN RESPECT OF THE FAILURE OF ANY POSTER OR THIRD PARTY TO MAKE ANY PAYMENT. ANY
EXPERT OR THIRD PARTY THAT HAS CLAIMS IN RESPECT OF UNPAID AMOUNTS MUST DIRECT
SUCH CLAIMS TO THE APPLICABLE PAYOR, AND CODERSCLAN SHALL NOT HAVE ANY
LIABILITY IN RESPECT THEREOF.</span></p><br/><p><span>You understand that CodersClan is
not responsible for the accuracy, usefulness, safety, appropriateness of or infringement
by any User Content available on the Site.&nbsp; Although users must agree to
these Terms, it is possible that other users (including unauthorized users) may
post or transmit offensive or obscene materials that you may be involuntarily
exposed to such offensive or obscene materials, and you hereby agree to waive,
and do waive, any legal or equitable rights or remedies you have or may have
against CodersClan with respect to thereto. It is also possible for others to
obtain personal information about you due to your use of the Site, including through
any User Content that you make available through your account. Anyone receiving
or viewing User Content may use your information (such as your contact details,
location or description of materials that are made available) for purposes
other than what you intended. We are not responsible for the use of any
personal information that you disclose on the Site or through any User Content.
By making any information (including your contact details, location or
description of materials) available through the Site you acknowledge that you
understand and have agreed to such risks. WE DISCLAIM ALL LIABILITY, REGARDLESS
OF THE FORM OF ACTION, FOR THE ACTS OR OMISSIONS OF OTHER USERS (INCLUDING
UNAUTHORIZED USERS), WHETHER SUCH ACTS OR OMISSIONS OCCUR DURING THE USE OF THE
SITE OR OTHERWISE.</span></p><br/><p><span>In addition, CodersClan acts as an
intermediary and makes connections between various users but cannot and does
not accept any responsibility for the actions of any Posters, Experts or other
users. WE DISCLAIM ALL LIABILITY FOR ANY FAILURE OF A POSTER TO MAKE PAYMENT OF
ANY BOUNTRY OR SATISFY ANY OTHER OBLIGATIONS OF SUCH POSTER. WE DISCLAIM ALL
LIABILITY FOR THE FAILURE OF ANY SOLUTION TO SATISFY ANY SPECIFIATIONS,
REQUIREMENTS OR WARRANTIES AND REPRESENTATIONS SET FORTH IN THESE TERMS, OR AN
EXPERT’S FAILURE TO SATISFY ANY OF ITS OBLIGATIONS. IF YOU USE ANY SOLUTION ON
THIS SITE, THEN YOU DO AT YOUR OWN RISK AND CODERSCLAN SHALL NOT HAVE ANY
LIABILITY THEREFOR.</span></p><br/><p><span>No advice or information, whether
oral or written, obtained by you from CodersClan, shall create any warranty not
expressly stated in these Terms. If you choose to rely on such information, you
do so solely at your own risk. Some states or jurisdictions do not allow the
exclusion of certain warranties. Accordingly, some of the above exclusions may
not apply to you.</span></p><br/><h4>Limitation of Liability</h4><br/><p><span>IN NO EVENT SHALL CODERSCLAN OR ANY
OF ITS OFFICERS, DIRECTORS, EMPLOYEES, OR AGENTS BE LIABLE TO YOU FOR ANY
DAMAGES WHATSOEVER, INCLUDING WITHOUT LIMITATION, INDIRECT, INCIDENTAL,
SPECIAL, PUNITIVE, AND/OR CONSEQUENTIAL DAMAGES, ARISING OUT OF OR IN
CONNECTION WITH YOUR USE OF THE<span className="uppercase">Site</span>OR
            USER CONTENT, INCLUDING BUT NOT LIMITED TO THE QUALITY, ACCURACY, OR UTILITY OF
            THE INFORMATION PROVIDED AS PART OF OR THROUGH THE<span className="uppercase">Site</span>, WHETHER THE DAMAGES ARE FORESEEABLE AND WHETHER OR NOT CODERSCLAN
            HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. THE FOREGOING LIMITATION
            OF LIABILITY SHALL APPLY TO THE FULLEST EXTENT PERMITTED BY LAW IN THE
            APPLICABLE JURISDICTION AND IN NO EVENT SHALL CODERSCLAN'S CUMULATIVE LIABILITY
            TO YOU EXCEED AMOUNTS PAID TO CODERSCLAN FOR USE OF THE<span className="uppercase">Site</span>. IF YOU HAVE NOT MADE ANY PAYMENTS TO CODERSCLAN FOR THE
            USE OF THE<span className="uppercase">Site</span>, THEN CODERSCLAN
            SHALL NOT HAVE ANY LIABILITY TOWARDS YOU.</span></p><br/><p><span>In addition to the foregoing, CodersClan
assumes no responsibility for any error, omission, interruption, deletion,
defect, delay in operation or transmission, communications line failure, theft
or destruction or unauthorized access to, or alteration of, any User Content. CodersClan
is not responsible for any problems or technical malfunction of any telephone or
cellular phone network or lines, computer online systems, servers or providers,
computer equipment, software, failure of any email due to technical problems or
traffic congestion on the Internet or on the Site, including any injury or
damage to users or to any person's mobile device or computer related to or
resulting from participation or downloading materials in connection with the Site.
UNDER NO CIRCUMSTANCES SHALL CODERSCLAN BE RESPONSIBLE FOR ANY LOSS OR DAMAGE,
INCLUDING PERSONAL INJURY OR DEATH, RESULTING FROM USE OF THE SITE, FROM ANY USER
CONTENT POSTED ON OR THROUGH THE SITE, OR FROM THE CONDUCT OF ANY USERS OF THE SITE,
WHETHER ONLINE OR OFFLINE, OR IN CONNECTION WITH ANY BORROWING OR LENDING.</span></p><br/><p><span>CodersClan cannot and does not take
any responsibility or liability in respect of the actions, errors or omissions
of any users. PLEASE BE AWARE THAT CODERSCLAN HAS NOT INVESTIGATED THE
BACKGROUND OR HISTORY OF ANY<span className="uppercase">POSTER,
EXPERT OR OTHER USER</span>AND DOES NOT HAVE THE CAPABILITY OF DOING SO. AS
            SUCH, YOUR INTERACTION WITH ANY OTHER USER,<span className="uppercase">including,
without limitation, during communications, meetings, agreements or other
dealings, INCLUDING IN</span>THE CONTEXT OF LENDING OR BORROWING OR
            NEGOTIATING THE SAME, IS AT YOUR OWN RISK. PLEASE BE AWARE THAT CODERSCLAN HAS
            NOT INVESTIGATED THE<span className="uppercase">suitability OR</span>            SAFETY OF ANY OF THE OFFERED MATERIALS. AS SUCH, USE OF THE OFFERED MATERIALS
            IS AT YOUR OWN RISK.</span></p><br/><h4>Indemnification</h4><br/><p><span>You agree to indemnify, defend, and
hold harmless CodersClan and its respective employees, directors, officers,
subcontractors and agents of each, against any and all claims, damages, or
costs or expenses (including court costs and attorneys’ fees) that arise
directly or indirectly from: (a) br/each of these Terms by you or anyone using
your computer or password; (b) any claim, loss or damage experienced from your
use or attempted use of (or inability to use) the Site, including any transactions
that you conduct or attempt; (c) your violation of any law or regulation; (d)
your infringement of any right of any third party; and (e) any other matter for
which you are responsible hereunder or under law. You agree that your use of
the Site, including, without limitation, provision of services in connection
with the Site shall be in compliance with all applicable laws, regulations and
guidelines.</span></p><br/><h4>Miscellaneous</h4><br/><p><span>These Terms shall be governed by the
law of the State of New York exclusive of its choice of law rules. Your conduct
may also be subject to other local, state, and national laws. Any action to be
br/ought in connection with these Terms or the Application shall be br/ought
exclusively in the courts in the City of New York and you irrevocably consent
to their jurisdiction.<strong><em></em></strong>Any cause of action against CodersClan
            must be br/ought within one (1) year of the date such cause of action arose. In
            the event that any provision of these Terms is held to be unenforceable, such
            provision shall be replaced with an enforceable provision which most closely
            achieves the effect of the original provision, and the remaining terms of these
            Terms shall remain in full force and effect. Nothing in this Agreement creates
            any agency, employment, joint venture, or partnership relationship between you
            and CodersClan, or between you and any other user of CodersClan, or authorizes
            you to act on behalf of CodersClan or any other user of CodersClan. Except as
            may be expressly stated in these Terms, these Terms constitute the entire
            agreement between CodersClan and you, and between you and other users of
            CodersClan, pertaining to the subject matter hereof. CodersClan may assign its
            rights and obligations hereunder to any third party without prior notice. A
            Poster, solely to the extent acting in the capacity of a Poster on the Site,
            may assign its rights and obligations hereunder to any third party without
            prior notice. An Expert, to the extent not acting in the capacity of an Expert
            on the Site, may not assign any of its rights or obligations hereunder, and any
            assignment in violation of the foregoing shall be void. No waiver of any br/each
            or default hereunder shall be deemed to be a waiver of any preceding or
            subsequent br/each or default. If we are required to provide notice to you
            hereunder, we may provide such notice to the email account (or other account of
            any service or social network) that you provided upon registration, and such
            notice shall be deemed received by you upon our transmission thereof. Any claim
            against ClodersClan must be br/ought by you in your individual capacity, and not
            as a plaintiff or class member in any purported class, collective,
            representative, multiple plaintiff, or similar proceeding (“<strong>Class Action</strong>”).
            The parties expressly waive any ability to maintain any Class Action in any
            forum.</span></p><br/><p><em>Last updated: March 2014</em></p>
      </div>
    </Terms>
  </div>
);