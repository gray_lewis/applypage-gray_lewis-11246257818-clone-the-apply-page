import React from 'react'
import styled from 'styled-components'
import {Colors} from "../consts/styles";
//

const AgreementSuccess = styled.section`
  background-color: ${ Colors.LIGHT_GRAY };
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 5rem 0;
  height: 100vh;
  box-sizing: border-box;
  
  text-align: center;
  
  h1{
    color: ${ Colors.LIGHT_BLACK } !important;
    margin-bottom: 1rem;
  }
  
  p{
    font-weight: bold;
    margin-bottom: 2rem;
    font-size: 1.25rem !important;
  }
  
  @media (max-width: 415px){
    
    box-sizing: content-box;
    padding: 2rem 0;
    
    img{
      width: 100%;
    }
  }
  
  @media (max-width: 321px){
    padding: 5rem 0;
  }
  
`;

export default () => (
  <AgreementSuccess>
    <div className="container">
      <h1>Thank You for Signing Up!</h1>
      <p>We're looking forward to working together! A member of our team should reach out soon :)</p>
      <div>
        <img src="https://media.giphy.com/media/l3q2Z6S6n38zjPswo/giphy.gif" alt=""/>
      </div>
    </div>
  </AgreementSuccess>
);
