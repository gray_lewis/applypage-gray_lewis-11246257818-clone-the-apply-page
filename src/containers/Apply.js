import React from 'react'
import styled from 'styled-components';
import { SiteData, RouteData } from 'react-static';
import { Colors } from '../consts/styles';
//

const Apply = styled.section`
  background-color: ${ Colors.LIGHT_GRAY };
  display: flex;
  justify-content: center;
  padding: 6rem 0;
  
  h1{
    color: ${ Colors.LIGHT_BLACK } !important;
    font-weight: 400 !important;
    font-size: 2rem !important;
  }
  
  strong{
    font-weight: bold;
  }

  .container {
    position: relative;
    width: 1000px;
    z-index: 1;
    height: 100vh;
    overflowScrolling: "touch",
    WebkitOverflowScrolling: "touch"
  }
  
  .uppercase{
    text-transform: uppercase;
  }

  iframe{ 
    position: absolute; 
    left:0; 
    right:0; 
    bottom:0; top:0; 
    border:0; 
    min-height: 500px; 
    height: 100vh; 
  }
`

function Variation(props) {
  const variation = `${props.variation}`;
  const links = props.links;

  return <iframe title="form" id="typeform-full" width="100%" height="100%" frameBorder="0" src={links[variation]} />
}

export default () => {
  return (
  <SiteData>
    {({ links }) => (
      <RouteData>
        {({ variation }) => (
          <Apply>
            <div className="container">
              <Variation variation={ variation } links={ links }/>
              <script type="text/javascript" src="https://embed.typeform.com/embed.js" />
            </div>
          </Apply>
          )}
        </RouteData>
    )}
  </SiteData>
  )
}
