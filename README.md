# CodersClan Light Version
This can be run both with Yarn or NPM. If ran with NPM, make sure to add "run" before "state", "build" and "serve". 
#### Run locally
`yarn install`
`yarn start`

#### Production preview
`yarn state`

#### Build
`yarn build`

#### Serve
`yarn serve`
